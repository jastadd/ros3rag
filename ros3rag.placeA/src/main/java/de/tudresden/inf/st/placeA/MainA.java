package de.tudresden.inf.st.placeA;

import de.tudresden.inf.st.placeA.ast.MqttHandler;
import de.tudresden.inf.st.placeA.ast.Scene;
import de.tudresden.inf.st.placeA.ast.WorldModelA;
import de.tudresden.inf.st.ros3rag.common.SharedMainParts;
import de.tudresden.inf.st.ros3rag.common.Util;

import java.io.File;
import java.io.IOException;

import static de.tudresden.inf.st.ros3rag.common.Util.mqttUri;
import static de.tudresden.inf.st.ros3rag.common.Util.readScene;

/**
 * Entry point for RAG model in place A.
 *
 * @author rschoene - Initial contribution
 */
public class MainA extends SharedMainParts<MqttHandler, WorldModelA> {
  private final String TOPIC_SCENE_UPDATE_FROM_ROS;
  private final String TOPIC_SCENE_UPDATE_TO_PLACE_B;

  private final String TOPIC_DEMO_MOVE_objectRed1_BLUE;
  private final String TOPIC_DEMO_MOVE_objectRed1_RED;
  private final String TOPIC_EVAL_MOVE;

  MainA(String configFile) {
    super("place-a", UtilA.pathToDirectoryOfPlaceA().resolve(configFile));
    this.TOPIC_SCENE_UPDATE_FROM_ROS = cellName + "/scene/update";
    this.TOPIC_SCENE_UPDATE_TO_PLACE_B = cellName + "/logical/update";

    this.TOPIC_DEMO_MOVE_objectRed1_BLUE = cellName + "/demo/move/objectRed1/blue";
    this.TOPIC_DEMO_MOVE_objectRed1_RED = cellName + "/demo/move/objectRed1/red";
    this.TOPIC_EVAL_MOVE = cellName + "/eval/move";
  }

  public static void main(String[] args) throws Exception {
    String configFile = args.length == 0 ? "src/main/resources/config-a.yaml" : args[0];
    new MainA(configFile).run();
  }

  @Override
  protected MqttHandler createMqttHandler() {
    return new MqttHandler("mainHandlerA");
  }

  @Override
  protected void createSpecificMainHandlerConnections() {
    mainHandler.newConnection(TOPIC_DEMO_MOVE_objectRed1_BLUE, bytes ->
        UtilA.updatePositionOfObjectToLocation(model.getScene(), "objectRed1", "binBlue")
    );
    mainHandler.newConnection(TOPIC_DEMO_MOVE_objectRed1_RED, bytes ->
        UtilA.updatePositionOfObjectToLocation(model.getScene(), "objectRed1", "binRed")
    );
    mainHandler.newConnection(TOPIC_EVAL_MOVE, bytes -> {
        String[] tokens = new String(bytes).split(" to ");
        String objectName = tokens[0];
        String locationName = tokens[1];
        UtilA.updatePositionOfObjectToLocation(model.getScene(), objectName, locationName);
    });
  }

  @Override
  protected WorldModelA createWorldModel() throws Exception {
    de.tudresden.inf.st.ceti.Scene scene = readScene(
        UtilA.pathToDirectoryOfPlaceA().resolve(config.forA.filenameInitialScene)
    );
    Scene myScene = UtilA.convert(scene);
    return new WorldModelA().setScene(myScene);
  }

  @Override
  protected void readInitialConfigs() throws IOException {
    // read and set regions
    File regionFile = UtilA.pathToDirectoryOfPlaceA().resolve(config.filenameRegions).toFile();
    UtilA.setRegions(model, Util.parseRegionConfig(regionFile));

    // no robots to be set
  }

  @Override
  protected void connectEndpoints() throws IOException {
    model.connectScene(mqttUri(TOPIC_SCENE_UPDATE_FROM_ROS, config));
    model.connectLogicalScene(mqttUri(TOPIC_SCENE_UPDATE_TO_PLACE_B, config), true);
  }

  @Override
  protected String getModelInfos(WorldModelA worldModelA, boolean detailed) {
    return UtilA.getModelInfos(model, detailed);
  }
}
