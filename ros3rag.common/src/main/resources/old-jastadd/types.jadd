import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import java.util.*;

aspect Resolving {
  //--- resolveObjectOfInterest ---
  syn ObjectOfInterest Scene.resolveObjectOfInterest(String name) {
    for (DropOffLocation location : getDropOffLocationList()) {
      if (location.getName().equals(name)) {
        return location;
      }
    }
    for (MovableObject movableObject : getMovableObjectList()) {
      if (movableObject.getName().equals(name)) {
        return movableObject;
      }
    }
    for (RobotObject robotObject : getRobotObjectList()) {
      if (robotObject.getName().equals(name)) {
        return robotObject;
      }
    }
    return null;
  }

  //--- resolveLogicalObjectOfInterest ---
  syn LogicalObjectOfInterest LogicalScene.resolveLogicalObjectOfInterest(String name) {
    for (LogicalRegion region : getLogicalRegionList()) {
      if (region.getName().equals(name)) {
        return region;
      }
    }
    for (LogicalMovableObject movableObject : getLogicalMovableObjectList()) {
      if (movableObject.getName().equals(name)) {
        return movableObject;
      }
    }
    return null;
  }

  syn DropOffLocation LogicalMovableObject.myLocation() {
    // classical reference attribute (was relation, but not allowed with RagConnect)
    ObjectOfInterest obj = containingScene().resolveObjectOfInterest(getNameOfMyLocation());
    return obj == null ? null : obj.asDropOffLocation();
  }
}

aspect Computation {
  //--- isLocatedAt ---
  syn boolean MovableObject.isLocatedAt(DropOffLocation location) {
    Orientation orient = location.getOrientation();
    Position locationPosition = location.getPosition();
    // true to do normalization, first parameter is scalar value
    Rotation locationRotation = new Rotation(orient.getW(), orient.getX(), orient.getY(), orient.getZ(), true);

    Position pos = this.getPosition();
    // first do translation of objectVector to coordinate system of location
    Vector3D objectVector = new Vector3D(pos.getX() - locationPosition.getX(),
                                         pos.getY() - locationPosition.getY(),
                                         pos.getZ() - locationPosition.getZ());

    // apply rotation on this point
    Vector3D rotatedObjectVector = locationRotation.applyInverseTo(objectVector);

    // check whether coordinates are within the bounds of the given location
    Size locationSize = location.getSize();
    // we use half of the size of to the position as it is in the center of the location
    double halfLocationLength = 0.5 * locationSize.getLength();
    double halfLocationWidth = 0.5 * locationSize.getWidth();
//    double halfLocationHeight = 0.5 * locationSize.getHeight();

    double rotatedX = rotatedObjectVector.getX();
    double rotatedY = rotatedObjectVector.getY();
    double rotatedZ = rotatedObjectVector.getZ();

//    System.out.printf("min: (%f , %f , %f). rotated: (%f , %f , %f), max: (%f , %f , %f)%n",
//      -halfLocationLength, -halfLocationWidth, -halfLocationHeight,
//      rotatedX, rotatedY, rotatedZ,
//      halfLocationLength,
//      halfLocationWidth,
//      halfLocationHeight);

    return  LT(-halfLocationLength, rotatedX) && LT(rotatedX, halfLocationLength) &&
       LT(-halfLocationWidth, rotatedY) && LT(rotatedY, halfLocationWidth); // &&
       //LT(-halfLocationHeight, rotatedZ) && LT(rotatedZ, halfLocationHeight);
  }

  syn MovableObject DropOffLocation.getObjectLocatedHere() {
    for (MovableObject obj : containingScene().getMovableObjects()) {
      if (obj.isLocatedAt(this)) {
        return obj;
      }
    }
    return null;
  }

  //--- getLogicalScene ---
  syn LogicalScene Scene.getLogicalScene() {
    var result = new LogicalScene();
    Map<MovableObject, LogicalMovableObject> objects = new HashMap<>();
    for (MovableObject movableObject : getMovableObjectList()) {
      LogicalMovableObject newLogicalMovableObject = new LogicalMovableObject();
      newLogicalMovableObject.setName(movableObject.getName());
      result.addLogicalMovableObject(newLogicalMovableObject);
      objects.put(movableObject, newLogicalMovableObject);
    }
    for (Region region : regionList()) {
      LogicalRegion logicalRegion = new LogicalRegion();
      logicalRegion.setName(region.getName());
      result.addLogicalRegion(logicalRegion);
      for (MovableObject movableObject : getMovableObjectList()) {
        for (DropOffLocation location : region.locationList()) {
          if (movableObject.isLocatedAt(location)) {
            LogicalMovableObject logicalObject = objects.get(movableObject);
            logicalRegion.addContainedObject(logicalObject);
            logicalObject.setNameOfMyLocation(location.getName());
          }
        }
      }
    }
    return result;
  }

  private static final double MovableObject.DELTA = 0.0001;

  //--- LT ---
  /**
   * @return d1 <= d2 (within a DELTA)
   */
  private boolean MovableObject.LT(double d1, double d2) {
    return org.apache.commons.math3.util.Precision.compareTo(d1, d2, DELTA) <= 0;
  }
}

aspect Navigation {
  // --- isDropOffLocation ---
  syn boolean ObjectOfInterest.isDropOffLocation() = false;
  eq DropOffLocation.isDropOffLocation() = true;

  // --- asDropOffLocation ---
  syn DropOffLocation ObjectOfInterest.asDropOffLocation() = null;
  eq DropOffLocation.asDropOffLocation() = this;

  // --- isRobotObject ---
  syn boolean ObjectOfInterest.isRobotObject() = false;
  eq RobotObject.isRobotObject() = true;

  // --- asRobotObject ---
  syn RobotObject ObjectOfInterest.asRobotObject() = null;
  eq RobotObject.asRobotObject() = this;

  // --- isLogicalRegion ---
  syn boolean LogicalObjectOfInterest.isLogicalRegion() = false;
  eq LogicalRegion.isLogicalRegion() = true;

  // --- asLogicalRegion ---
  syn LogicalRegion LogicalObjectOfInterest.asLogicalRegion() = null;
  eq LogicalRegion.asLogicalRegion() = this;

  // --- isLogicalMovableObject ---
  syn boolean LogicalObjectOfInterest.isLogicalMovableObject() = false;
  eq LogicalMovableObject.isLogicalMovableObject() = true;

  // --- asLogicalMovableObject ---
  syn LogicalMovableObject LogicalObjectOfInterest.asLogicalMovableObject() = null;
  eq LogicalMovableObject.asLogicalMovableObject() = this;

  inh Scene DropOffLocation.containingScene();
  inh Scene MovableObject.containingScene();
  inh Scene LogicalMovableObject.containingScene();
  eq Scene.getChild().containingScene() = this;

  syn boolean LogicalMovableObject.hasLocatedAt() = !getLocatedAtList().isEmpty();

  // must be "implemented" in concrete world models, i.e., an equation must be defined
  inh JastAddList<Region> Scene.regionList();

  syn List<Region> DropOffLocation.containedInRegion() {
    List<Region> result = new ArrayList<>();
    for (Region region : containingScene().regionList()) {
      List<String> locationNames = Arrays.asList(region.getLocationNames().split(","));
      if (locationNames.contains(getName())) {
        result.add(region);
      }
    }
    return result;
  }

  protected static List<String> ASTNode.arrayAsList(String array) {
    if (array == null || array.length() == 0) {
      return new ArrayList<>();
    }
    return new ArrayList<>(Arrays.asList(array.split(",")));
  }
  syn List<String> Region.locationNamesAsList() = arrayAsList(getLocationNames()) ;
  syn List<DropOffLocation> Region.locationList();

  syn Set<String> LogicalMovableObject.regionNameSet() {
    return getLocatedAtList().stream().map(LogicalRegion::getName).collect(java.util.stream.Collectors.toSet());
  }
}

aspect Printing {
  //--- prettyPrint ---
  syn String JastAddList.prettyPrint() {
    return prettyPrint(Object::toString);
  }
  syn String JastAddList.prettyPrint(java.util.function.Function<T, String> toString) {
    return java.util.stream.StreamSupport.stream(Spliterators.spliteratorUnknownSize(this.iterator(), 16), false).map(toString::apply).collect(java.util.stream.Collectors.joining(", ", "[", "]"));
  }

  syn String Scene.prettyPrint() {
    StringBuilder sb = new StringBuilder();
    if (getNumDropOffLocation() == 0) {
      sb.append(" no locations\n");
    } else {
      for (var location : getDropOffLocationList()) {
        sb.append(" location ").append(location.prettyPrint()).append("\n");
      }
    }
    if (getNumMovableObject() == 0) {
      sb.append(" no objects\n");
    } else {
      for (var obj : getMovableObjectList()) {
        sb.append(" obj ").append(obj.prettyPrint()).append("\n");
      }
    }
    return sb.toString();
  }

  syn String MovableObject.prettyPrint() {
    return "<obj " + nameAndHash() + getPosition().prettyPrint() + getOrientation().prettyPrint() + getSize().prettyPrint() + ">";
  }

  syn String DropOffLocation.prettyPrint() {
    return "{" + printPrefix() + " " + getName() + getPosition().prettyPrint() + getOrientation().prettyPrint() + getSize().prettyPrint() + "}";
  }

  syn String DropOffLocation.printPrefix() = "loc";
  eq CollaborationZone.printPrefix() = "CZ";

  syn String Region.prettyPrint() {
    return "{reg " + nameAndHash() + "}";
  }

  syn String Position.prettyPrint() {
    return " pos=(" + getX() + "," + getY() + "," + getZ() + ")";
  }

  syn String Orientation.prettyPrint() {
    return " orient=(" + getX() + "," + getY() + "," + getZ() + "," + getW() + ")";
  }

  syn String Size.prettyPrint() {
    return " size=(" + getLength() + "," + getWidth() + "," + getHeight() + ")";
  }

  syn String LogicalScene.prettyPrint() {
    StringBuilder sb = new StringBuilder();
    if (getNumLogicalRegion() == 0) {
      sb.append(" no region\n");
    } else {
      // TODO
      for (LogicalRegion region : getLogicalRegionList()) {
        sb.append(" region ").append(region.prettyPrint());
        if (!region.getContainedObjectList().isEmpty()) {
          sb.append(" (objects: ").append(region.getContainedObjectList().stream().map(LogicalMovableObject::getName).collect(java.util.stream.Collectors.joining(","))).append(")");
        }
        sb.append("\n");
      }
    }
    if (getNumLogicalMovableObject() == 0) {
      sb.append(" no objects\n");
    } else {
      for (LogicalMovableObject obj : getLogicalMovableObjectList()) {
        sb.append(" obj ").append(obj.prettyPrint());
        if (obj.hasLocatedAt()) {
          sb.append(" (locatedAt: ").append(obj.getLocatedAtList().stream().map(LogicalRegion::nameAndHash).collect(java.util.stream.Collectors.joining(","))).append(")");
        }
        sb.append("\n");
      }
    }
    return sb.toString();
  }

  syn String LogicalMovableObject.prettyPrint() {
    return  "<lObj " + nameAndHash() + ">";
  }
  syn String LogicalRegion.prettyPrint() {
    return "{lReg " + nameAndHash() + "}";
  }

  syn String LogicalObjectOfInterest.nameAndHash() = getName() + "@" + Integer.toHexString(hashCode());
  syn String ObjectOfInterest.nameAndHash() = getName() + "@" + Integer.toHexString(hashCode());
  syn String Region.nameAndHash() = getName() + "@" + Integer.toHexString(hashCode());
}

aspect ConvenienceMethods {
  // --- of ---
  public static DropOffLocation DropOffLocation.of(String name,
        Position position, Orientation orientation, Size size) {
    var location = new DropOffLocation();
    initObjectOfInterest(location, name, position, orientation, size);
    return location;
  }

  public static MovableObject MovableObject.of(String name, Position position) {
    var location = new MovableObject();
    initObjectOfInterest(location, name, position,
        Orientation.of(0, 0, 0, 0), Size.of(0, 0, 0));
    return location;
  }

  protected static void ObjectOfInterest.initObjectOfInterest(ObjectOfInterest object, String name,
        Position position, Orientation orientation, Size size) {
    object.setName(name);
    object.setPosition(position);
    object.setOrientation(orientation);
    object.setSize(size);
  }

  public static Position Position.of(double x, double y, double z) {
    return new Position(x, y, z);
  }

  public static Orientation Orientation.of(double x, double y, double z, double w) {
    return new Orientation(x, y, z, w);
  }

  public static Size Size.of(double length, double width, double height) {
    return new Size(length, width, height);
  }
}

aspect Glue {
  class MqttHandler implements de.tudresden.inf.st.ros3rag.common.SharedMainParts.MqttHandlerWrapper<MqttHandler> {}
}
