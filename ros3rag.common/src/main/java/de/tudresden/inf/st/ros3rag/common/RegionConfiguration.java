package de.tudresden.inf.st.ros3rag.common;

import java.util.List;

/**
 * Data class for region definitions.
 *
 * @author rschoene - Initial contribution
 */
public class RegionConfiguration {
  public List<RegionDefinition> regions;

  public static class RegionDefinition {
    public String name;
    public List<String> positions;
  }
}
