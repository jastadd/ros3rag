package de.tudresden.inf.st.ros3rag.common;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.function.Consumer;

/**
 * TODO: Add description.
 *
 * @author rschoene - Initial contribution
 */
public class SimpleMain {
  public static void main(String[] args)  {
    URI uri;
    try {
      uri = new URI("mqtt://localhost/my/topic");
    } catch (URISyntaxException e) {
      e.printStackTrace();
      return;
    }
//    URL url = new URL("mqtt://localhost/my/topic");
    Consumer<String> consumer = message -> {
      int i = 5;
      System.out.println(uri.getHost());
    };
    Runnable run = () -> System.out.println(consumer);
    Object o = new byte[4];
    Consumer<byte[]> c;

  }
}
