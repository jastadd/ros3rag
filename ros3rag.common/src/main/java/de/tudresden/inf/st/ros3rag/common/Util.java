package de.tudresden.inf.st.ros3rag.common;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.protobuf.util.JsonFormat;
import de.tudresden.inf.st.ceti.Object;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Helper method dealing with config.
 *
 * @author rschoene - Initial contribution
 */
public class Util {
  private static final Logger logger = LogManager.getLogger(Util.class);

  public static Configuration parseConfig(File configFile) throws IOException {
    logger.info("Using config file: {}", configFile.getAbsolutePath());
    ObjectMapper mapper = new ObjectMapper(
        new YAMLFactory().configure(JsonParser.Feature.ALLOW_YAML_COMMENTS, true)
    );
    return mapper.readValue(configFile, Configuration.class);
  }

  public static ConfigurationScaling parseScalingConfig(File configFile) throws IOException {
    logger.info("Using config file: {}", configFile.getAbsolutePath());
    ObjectMapper mapper = new ObjectMapper(
        new YAMLFactory().configure(JsonParser.Feature.ALLOW_YAML_COMMENTS, true)
    );
    return mapper.readValue(configFile, ConfigurationScaling.class);
  }

  public static RegionConfiguration parseRegionConfig(File regionConfigFile) throws IOException {
    logger.info("Using region config file: {}", regionConfigFile.getAbsolutePath());
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(regionConfigFile, RegionConfiguration.class);
  }

  public static String mqttUri(String topic, Configuration config) {
    return mqttUri(topic, config.mqttHost);
  }

  public static String mqttUri(String topic, String mqttHost) {
    return "mqtt://" + mqttHost + "/" + topic;
  }

  public static de.tudresden.inf.st.ceti.Scene readScene(java.nio.file.Path path) throws java.io.IOException {
    logger.debug("Reading scene from {}", path.toAbsolutePath());
    var jsonString = Files.readString(path);
    var builder = de.tudresden.inf.st.ceti.Scene.newBuilder();
    JsonFormat.parser().merge(jsonString, builder);
    return builder.build();
  }

  public static List<String> extractRobotNames(de.tudresden.inf.st.ceti.Scene scene) {
    List<String> result = new ArrayList<>();
    for (var obj : scene.getObjectsList()) {
      if (obj.getType().equals(Object.Type.ARM)) {
        result.add(obj.getId());
      }
    }
    return result;
  }

  public static Path pathToCommonDirectory() {
    return pathToModuleDirectory("ros3rag.common");
  }

  private final static Set<String> modules = new HashSet<>() {{
    add("ros3rag.placeA");
    add("ros3rag.placeB");
    add("ros3rag.old.a");
    add("ros3rag.old.b");
    add("ros3rag.common");
    add("ros3rag.coordinator");
  }};

  public static Path pathToModuleDirectory(String moduleName) {
    Path current = Paths.get("").toAbsolutePath();
    String currentFileName = current.getFileName().toString();
    Path repoRoot;
    if (modules.contains(currentFileName)) {
      // we are in some module, use parent
      repoRoot = current.getParent();
    } else if (current.resolve(moduleName).toFile().exists()) {
      repoRoot = current;
    } else {
      throw new IllegalStateException("Could not find ros3rag.common directory");
    }
    return repoRoot.resolve(moduleName);
  }
}
