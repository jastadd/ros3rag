package de.tudresden.inf.st.ros3rag.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Shared parts of both main classes.
 *
 * @author rschoene - Initial contribution
 */
public abstract class SharedMainParts<MqttHandler extends SharedMainParts.MqttHandlerWrapper<MqttHandler>,
    WorldModel extends SharedMainParts.WorldModelWrapper> {
  protected final Logger logger = LogManager.getLogger(this.getClass());

  private final String TOPIC_MODEL;
  private final String TOPIC_STATUS;
  private final String TOPIC_REWIND;
  private final String TOPIC_EXIT;

  public static final String TOPIC_SUFFIX_COORDINATOR_STATUS = "status";
  public static final String TOPIC_SUFFIX_COORDINATOR_COMMAND = "command";

  protected MqttHandler mainHandler;
  protected WorldModel model;
  protected Configuration config;
  protected final String cellName;
  protected final Path pathToConfig;
  private CountDownLatch startCondition;

  public SharedMainParts(String cellName, Path pathToConfig) {
    this.cellName = cellName;
    this.pathToConfig = pathToConfig;

    this.TOPIC_MODEL = cellName + "/model";
    this.TOPIC_STATUS = cellName + "/status";
    this.TOPIC_REWIND = cellName + "/rewind";
    this.TOPIC_EXIT = cellName + "/exit";
  }

  private Map<String, String> channelDescriptions() {
    return new LinkedHashMap<>() {{
      put(TOPIC_MODEL, "Print current model (detailed if message starts with 'detail')");
      put(TOPIC_REWIND, "Rewind app to start");
      put(TOPIC_EXIT, "Exit app");
    }};
  }

  public void run() throws Exception {
    config = Util.parseConfig(pathToConfig.toFile());

    /// Prepare main handler
    mainHandler = createMqttHandler().dontSendWelcomeMessage();
    mainHandler.setHost(config.mqttHost);
    if (!mainHandler.waitUntilReady(2, TimeUnit.SECONDS)) {
      logger.fatal("Could not connect to " + config.mqttHost + " -> Exiting");
      return;
    }
    CountDownLatch exitCondition = new CountDownLatch(1);
    mainHandler.newConnection(TOPIC_EXIT, bytes -> {
      logger.info("Got exit command");
      exitCondition.countDown();
      startCondition.countDown();
      logger.debug("exit latch count = {}, start latch count= {}",
          exitCondition.getCount(), startCondition.getCount());
    });
    mainHandler.newConnection(TOPIC_MODEL, bytes -> logStatus(new String(bytes)));
    mainHandler.newConnection(TOPIC_REWIND, bytes ->
        new Thread(() -> {
          {
            try {
              rewind("rewind");
            } catch (Exception e) {
              logger.catching(e);
            }
          }
        }).start()
    );
    if (config.useCoordinator()) {
      logger.info("Using coordinator logic");
      mainHandler.newConnection(joinTopics(config.coordinatorMqttTopicPrefix, TOPIC_SUFFIX_COORDINATOR_COMMAND),
          bytes -> reactToCoordinatorCommand(new String(bytes)));
    }

    createSpecificMainHandlerConnections();

    logger.info("Supported commands: {}", channelDescriptions());

    rewind("Start");

    Runtime.getRuntime().addShutdownHook(new Thread(this::close));

    exitCondition.await();
    logger.debug("After exit condition");
  }

  public static String joinTopics(String... topics) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < topics.length; i++) {
      String topic = topics[i];
      if (i > 0 && topic.startsWith("/")) {
        topic = topic.substring(1);
      }
      if (i < topics.length - 1 && topic.endsWith("/")) {
        topic = topic.substring(0, topic.length() - 1);
      }
      if (i > 0) {
        sb.append("/");
      }
      sb.append(topic);
    }
    return sb.toString();
  }

  private void reactToCoordinatorCommand(String command) {
    logger.debug("Got coordinator command {} for {}", command, cellName);
    switch (command) {
      case "rewind":
        try {
          rewind(command);
        } catch (Exception e) {
          e.printStackTrace();
        }
        break;
      case "start":
        startCondition.countDown();
        break;
      default:
        logger.warn("Unknown coordinator command {} for {}", command, cellName);
    }
  }

  private void rewind(String statusMessage) throws Exception {
    if (model != null) {
      logger.debug("Closing previous connections for {}", cellName);
      model.ragconnectCloseConnections();
    }

    config = Util.parseConfig(pathToConfig.toFile());

    logger.debug("Creating world model for {}", cellName);
    model = createWorldModel();

    logger.debug("Reading robots for {}", cellName);
    readInitialConfigs();

    logger.debug("Setup model connection for {}", cellName);
    model.ragconnectCheckIncremental();
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    if (config.useCoordinator()) {
      logger.debug("Awaiting start signal from coordinator for {}", cellName);
      startCondition = new CountDownLatch(1);
      mainHandler.publish(joinTopics(config.coordinatorMqttTopicPrefix, TOPIC_SUFFIX_COORDINATOR_STATUS),
          "up".getBytes(StandardCharsets.UTF_8));
      startCondition.await();
    }

    logger.debug("Connecting endpoints for {}", cellName);
    connectEndpoints();

    logStatus(statusMessage);

    if (config.useCoordinator()) {
      logger.debug("Publishing ready for {}", cellName);
      mainHandler.publish(joinTopics(config.coordinatorMqttTopicPrefix, TOPIC_SUFFIX_COORDINATOR_STATUS),
          "ready".getBytes(StandardCharsets.UTF_8));
    }
  }

  protected abstract MqttHandler createMqttHandler();

  protected abstract void createSpecificMainHandlerConnections();

  protected abstract WorldModel createWorldModel() throws Exception;

  protected abstract void readInitialConfigs() throws Exception;

  protected abstract void connectEndpoints() throws IOException;

  protected abstract String getModelInfos(WorldModel model, boolean detailed);

  private void logStatus(String message) {
    logger.info(message);
    String content = getModelInfos(model, message.equals("Start") || message.startsWith("detail"));
    logger.info("WorldModel\n{}", content);
    if (mainHandler != null) {
      mainHandler.publish(TOPIC_STATUS, content.getBytes(StandardCharsets.UTF_8));
    }
  }

  private void close() {
    logger.info("Exiting ...");
    mainHandler.close();
    model.ragconnectCloseConnections();
    logger.debug("After closing connections");
  }

  public interface MqttHandlerWrapper<SELF> {
    SELF dontSendWelcomeMessage();
    boolean newConnection(String topic, Consumer<byte[]> callback);
    void publish(String topic, byte[] payload);
    void close();

    SELF setHost(String host) throws java.io.IOException;

    @SuppressWarnings("UnusedReturnValue")
    boolean waitUntilReady(long value, TimeUnit unit);
  }

  public interface WorldModelWrapper {
    void ragconnectCheckIncremental();
    void ragconnectSetupMqttWaitUntilReady(long value, TimeUnit unit);
    void ragconnectCloseConnections();
  }
}
