package de.tudresden.inf.st.ros3rag.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * New Data class for initial configuration.
 *
 * @author rschoene - Initial contribution
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Configuration {
  public String mqttHost;
  public String filenameRegions;
  public String coordinatorMqttTopicPrefix;
  public ConfigurationForA forA;
  public ConfigurationForB forB;

  public static class ConfigurationForA {
    public String filenameInitialScene;
  }

  public static class ConfigurationForB {
    public String filenameReachability;
    public List<String> topicsSceneUpdate;
    public String topicCommand;
  }

  public boolean useCoordinator() {
    return coordinatorMqttTopicPrefix != null;
  }
}
