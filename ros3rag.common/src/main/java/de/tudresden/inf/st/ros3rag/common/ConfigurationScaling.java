package de.tudresden.inf.st.ros3rag.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * New Data class for initial configuration.
 *
 * @author rschoene - Initial contribution
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigurationScaling {
  public String mqttHost;
  public String coordinatorMqttTopicPrefix;
  public String topicLogicalUpdate;
  public int views;
  public int objects;
  public int robots;
  public int seed;
  public String distributionStrategy;

  public boolean useCoordinator() {
    return coordinatorMqttTopicPrefix != null;
  }
}
