#!/usr/bin/env bash
letter=$(echo $1 | tr a-z A-Z)
module="ros3rag.place${letter}"
shift
./gradlew :${module}:installDist && \
	./${module}/build/install/${module}/bin/${module} $@
