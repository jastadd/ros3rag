FROM openjdk:11

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y graphviz cloc && rm -rf /var/lib/apt/lists/*

ENV HOME /home/user
RUN useradd --create-home --home-dir $HOME user \
    && chmod -R u+rwx $HOME \
    && chown -R user:user $HOME

USER user

ENV GRADLE_USER_HOME /home/user/.gradle
COPY --chown=user:user gradle /temp/gradle
COPY --chown=user:user gradlew /temp/

WORKDIR /temp
RUN ./gradlew --no-daemon --version

COPY --chown=user:user . /ros3rag/

WORKDIR /ros3rag

RUN ./gradlew --no-daemon installDist

# target is either: ros3rag.placeA oder ros3rag.placeB
ENV TARGET unspecified
ENV CONFIG_FILE unspecified
ENTRYPOINT ["/bin/bash", "-c", "./${TARGET}/build/install/${TARGET}/bin/${TARGET} ${CONFIG_FILE}"]
