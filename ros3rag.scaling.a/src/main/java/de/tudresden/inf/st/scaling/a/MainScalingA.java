package de.tudresden.inf.st.scaling.a;

import de.tudresden.inf.st.jastadd.dumpAst.ast.Dumper;
import de.tudresden.inf.st.ros3rag.common.ConfigurationScaling;
import de.tudresden.inf.st.ros3rag.common.SharedMainParts;
import de.tudresden.inf.st.ros3rag.common.Util;
import de.tudresden.inf.st.scaling.a.ast.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static de.tudresden.inf.st.ros3rag.common.SharedMainParts.TOPIC_SUFFIX_COORDINATOR_STATUS;
import static de.tudresden.inf.st.ros3rag.common.SharedMainParts.joinTopics;

/**
 * Scaling Case Study - Site A.
 *
 * @author rschoene - Initial contribution
 */
public class MainScalingA {
  private static final Logger logger = LogManager.getLogger(MainScalingA.class);
  private static final String TOPIC_EXIT = "place-a/exit";

  private CompleteWorld world;
  private MqttHandler mainHandler;
  private ConfigurationScaling config;

  public static void main(String[] args) throws IOException, InterruptedException {
    new MainScalingA().run(args);
  }

  private void run(@SuppressWarnings("unused") String[] args) throws IOException, InterruptedException {
    logger.info("Hi from A");
    Path pathToConfig = Paths.get("..",
        "ros3rag.common", "src", "main", "resources", "config-scaling-mini.yml");
    config = Util.parseScalingConfig(pathToConfig.toFile());

    world = createWorld(config);
    mainHandler = new MqttHandler().dontSendWelcomeMessage().setHost(config.mqttHost);
    boolean mqttAvailable = mainHandler.waitUntilReady(2, TimeUnit.SECONDS);
    if (!mqttAvailable) {
      logger.fatal("Could not connect to " + config.mqttHost + " -> Exiting.");
      return;
    }

    generateAndSetViews();
    Dumper.read(world).dumpAsSVG(Paths.get("world-a.svg"));

    logger.info("Using coordinator logic");
    final String coordinatorPrefix = config.coordinatorMqttTopicPrefix + "a";
    mainHandler.newConnection(SharedMainParts.joinTopics(coordinatorPrefix,
            SharedMainParts.TOPIC_SUFFIX_COORDINATOR_COMMAND),
        bytes -> {
          connectMyScenes();
          mainHandler.publish(joinTopics(coordinatorPrefix, TOPIC_SUFFIX_COORDINATOR_STATUS),
              "ready".getBytes(StandardCharsets.UTF_8));
        });

    Runtime.getRuntime().addShutdownHook(new Thread(this::close));

    CountDownLatch exitCondition = new CountDownLatch(1);
    mainHandler.newConnection(TOPIC_EXIT, bytes -> {
      logger.info("Got exit command");
      exitCondition.countDown();
      logger.debug("exit latch count = {}",
          exitCondition.getCount());
    });

    mainHandler.publish(joinTopics(coordinatorPrefix, TOPIC_SUFFIX_COORDINATOR_STATUS),
        "up".getBytes(StandardCharsets.UTF_8));

    exitCondition.await();
  }

  private void connectMyScenes() {
    for (int viewIndex = 0; viewIndex < world.getConfig().getNumberOfViews(); viewIndex++) {
      try {
        world.connectView(
            Util.mqttUri(joinTopics(config.topicLogicalUpdate, String.valueOf(viewIndex)), config.mqttHost),
            viewIndex,
            true);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private static CompleteWorld createWorld(ConfigurationScaling input) {
    CompleteWorld result = new CompleteWorld();
    result.setConfig(new Config()
        .setNumberOfViews(input.views)
        .setNumberOfObjects(input.objects)
        .setNumberOfRobots(input.robots)
        .setSeed(input.seed)
    );
    return result;
  }

  private void generateAndSetViews() {
    Random rand = new Random(world.getConfig().getSeed());
    Map<Integer, Pair<LogicalScene, Map<Integer, LogicalRegion>>> viewAndRegionMap = new HashMap<>();
    for (int objectIndex = 0; objectIndex < world.getConfig().getNumberOfObjects(); objectIndex++) {
      LogicalMovableObject obj = new LogicalMovableObject();
      obj.setName("O" + objectIndex);
      // randomly pick a target region (essentially the last robot to place it)
      int targetRegionIndex = rand.nextInt(world.getConfig().getNumberOfRobots());
      // randomly pick a target view where this information is "stored"
      int targetViewIndex = rand.nextInt(world.getConfig().getNumberOfViews());

      Pair<LogicalScene, Map<Integer, LogicalRegion>> viewAndRegions = viewAndRegionMap.get(targetViewIndex);
      if (viewAndRegions == null) {
        viewAndRegions = new Pair<>(new LogicalScene(), new HashMap<>());
        viewAndRegionMap.put(targetViewIndex, viewAndRegions);
      }
      LogicalScene targetView = viewAndRegions._1;
      targetView.addLogicalMovableObject(obj);

      LogicalRegion targetRegion = viewAndRegions._2.get(targetRegionIndex);
      if (targetRegion == null) {
        targetRegion = new LogicalRegion();
        targetRegion.setName("Region" + targetRegionIndex);
        targetView.addLogicalRegion(targetRegion);
        viewAndRegions._2.put(targetRegionIndex, targetRegion);
      }

      targetRegion.addContainedObject(obj);
      obj.setNameOfMyLocation("L_" + targetRegion.getName()); // imaginary name of a location
    }

    for (int viewIndex = 0; viewIndex < world.getConfig().getNumberOfViews(); viewIndex++) {
      Pair<LogicalScene, Map<Integer, LogicalRegion>> pair = viewAndRegionMap.get(viewIndex);
      LogicalScene view = pair != null ? pair._1 : new LogicalScene();
      world.addView(view);
    }
  }

  private void close() {
    logger.info("Exiting ...");
    mainHandler.close();
    world.ragconnectCloseConnections();
  }
}
