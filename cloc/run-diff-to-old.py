#!/usr/bin/env python3
import difflib
import sys
import os


filesA = [
    ('WorldModelA.connect', 'OldWorldModelA.connect'),
    ('WorldModelA.jadd', 'OldWorldModelA.jadd'),
    ('WorldModelA.relast', 'OldWorldModelA.relast'),
]
currentDirA = 'ros3rag.placeA/src/main/jastadd'
oldDirA = 'ros3rag.old.a/src/main/jastadd'

filesB = [
    ('RobotReachabilityToBFS.jrag', 'OldRobotReachabilityToBFS.jrag'),
    ('RobotReachabilityToBFS.relast', 'OldRobotReachabilityToBFS.relast'),
    ('WorldModelB.connect', 'OldWorldModelB.connect'),
    ('WorldModelB.jadd', 'OldWorldModelB.jadd'),
    ('WorldModelB.relast', 'OldWorldModelB.relast'),
]
currentDirB = 'ros3rag.placeB/src/main/jastadd'
oldDirB = 'ros3rag.old.b/src/main/jastadd'

filesShared = [
    ('types.connect', 'types.connect'),
    ('types.jadd', 'types.jadd'),
    ('types.relast', 'types.relast')
]
currentDirShared = 'ros3rag.common/src/main/resources/jastadd'
oldDirShared = 'ros3rag.common/src/main/resources/old-jastadd'

comparing = [
    (currentDirA, oldDirA, filesA),
    (currentDirB, oldDirB, filesB),
    (currentDirShared, oldDirShared, filesShared),
]


def main():
    total_changed = 0
    seen = set()

    for currentDir, oldDir, files in comparing:
        print(currentDir, oldDir)
        for currentFilename, oldFilename in files:
            print(' ', currentFilename, oldFilename, end=': ')
            with open(os.path.join('..', currentDir, currentFilename)) as fdr:
                a = fdr.readlines()
            with open(os.path.join('..', oldDir, oldFilename)) as fdr:
                b = fdr.readlines()
            diff = difflib.unified_diff(a, b, lineterm='')
            changed_lines = [line for index, line in enumerate(diff) if index > 2 and line.startswith('+')]
            changed = len(changed_lines)
            print(changed)

            # put changed_lines into a file for later cloc call
            merged_filename = 'merged.' + currentFilename.rsplit('.', 2)[1]
            is_new_file = not merged_filename in seen
            print(f'>>{merged_filename} {is_new_file}')
            with open(merged_filename, 'w' if is_new_file else 'a') as fdr:
                fdr.writelines(changed_lines)
            seen.add(merged_filename)

            total_changed += changed

    print(f'total: {total_changed}')
    os.system(f'cloc-ag merged.*')


if __name__ == '__main__':
    main()
