package de.tudresden.inf.st.old.b;

import de.tudresden.inf.st.ceti.Object;
import de.tudresden.inf.st.old.b.ast.*;
import de.tudresden.inf.st.ros3rag.common.SharedMainParts;
import de.tudresden.inf.st.ros3rag.common.Util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

import static de.tudresden.inf.st.ros3rag.common.Util.mqttUri;

/**
 * Entry point for RAG model in place B.
 *
 * @author rschoene - Initial contribution
 */
public class OldMainB extends SharedMainParts<MqttHandler, ArtificialRoot> {
  private final String TOPIC_MODEL_SVG_PATH;

  private de.tudresden.inf.st.ceti.Scene demo_scene;

  OldMainB(String configFile) {
    super("place-b", OldUtilB.pathToDirectoryOfPlaceB().resolve(configFile));

    this.TOPIC_MODEL_SVG_PATH = cellName + "/model/svg/path";
  }

  public static void main(String[] args) throws Exception {
    String configFile = args.length == 0 ? "src/main/resources/config-b.yaml" : args[0];
    new OldMainB(configFile).run();
  }

  @Override
  public void run() throws Exception {
    // ensure that directory "images" exists
    File imagesDirectory = Paths.get("images/").toFile();
    if (!imagesDirectory.exists()) {
      boolean success = imagesDirectory.mkdir();
      if (!success) {
        logger.debug("Failed to create directory {}", imagesDirectory.getAbsolutePath());
      }
    }

    super.run();
  }

  @Override
  protected MqttHandler createMqttHandler() {
    return new MqttHandler("mainHandlerB");
  }

  @Override
  protected void createSpecificMainHandlerConnections() {
    mainHandler.newConnection("place-b/demo", bytes -> {
      String command = new String(bytes);
      int colonIndex = command.indexOf(":");
      if (colonIndex == -1) {
        logger.error("Unknown demo command {}", command);
        return;
      }
      String key = command.substring(0, colonIndex);
      String value = command.substring(colonIndex + 1);
      int slashIndex;
      switch (key) {
        case "scene":
          if (!value.equals("initial")) {
            logger.warn("Can only send initial scene, but got {}. Sending initial scene.", command);
          }
          try {
            demo_scene = Util.readScene(OldUtilB.pathToDirectoryOfPlaceB().resolve("src/main/resources/config-scene-b-placeworld-manual.json"));
            mainHandler.publish(config.forB.topicsSceneUpdate.get(0), demo_scene.toByteArray());
          } catch (IOException e) {
            e.printStackTrace();
          }
          break;
        case "robot":
          slashIndex = value.indexOf("/");
          String robot = value.substring(0, slashIndex);
          String stateString = "STATE_" + value.substring(slashIndex + 1).toUpperCase();
          Object.State state = Object.State.valueOf(stateString);
          updateAndPublishScene(robot, r -> r.toBuilder().setState(state).build());
          break;
        case "object":
          slashIndex = value.indexOf("/");
          String obj = value.substring(0, slashIndex);
          String location = value.substring(slashIndex + 1);
          Position pos = model.getWorldModelB().getMyScene().resolveObjectOfInterest(location).asDropOffLocation().getPosition();
          updateAndPublishScene(obj, o -> {
            Object.Builder builder = o.toBuilder();
            builder.getPosBuilder()
                .setX(pos.getX())
                .setY(pos.getY())
                .setZ(pos.getZ());
            return builder.build();
          });
          break;
        default:
          logger.error("Unknown demo command {}", command);
      }
    });
  }

  private void updateAndPublishScene(String objectName, Function<Object, Object> change) {
    demo_scene = OldUtilB.updateObject(demo_scene, objectName, change);
    mainHandler.publish(config.forB.topicsSceneUpdate.get(0), demo_scene.toByteArray());
  }

  @Override
  protected ArtificialRoot createWorldModel() {
    WorldModelB worldModel = new WorldModelB();
//    worldModel.ragconnectResetEvaluationCounter();
    worldModel.addOtherScene(new LogicalScene());
    return new ArtificialRoot().setWorldModelB(worldModel);
  }

  @Override
  protected void readInitialConfigs() throws Exception {
    model.getWorldModelB().setMyScene(new Scene());

    // read and set regions
    File regionBFile = OldUtilB.pathToDirectoryOfPlaceB().resolve(config.filenameRegions).toFile();
    OldUtilB.setRegions(model.getWorldModelB(), Util.parseRegionConfig(regionBFile));

    // init robots and reachability
    // assumption: robots do not change during runtime, so we have stable connections
    Path path = OldUtilB.pathToDirectoryOfPlaceB().resolve(Paths.get(config.forB.filenameReachability));
    OldReachabilityConfiguration reachability = OldUtilB.readReachability(path.toFile());

    for (OldReachabilityConfiguration.RobotConfiguration robotConfiguration : reachability.robots) {
      Robot robot = OldUtilB.createRobot(robotConfiguration.name);
      model.getWorldModelB().addRobot(robot);

      JastAddList<CanReachObjectOfInterest> reachabilityList = OldUtilB.convertToReachability(robotConfiguration.reachableLocations);
      robot.setCanReachObjectOfInterestList(reachabilityList);
    }
  }

  @Override
  protected void connectEndpoints() throws IOException {
    for (String topic : config.forB.topicsSceneUpdate) {
      checkSuccess(model.getWorldModelB().connectMySceneAsProtobuf(mqttUri(topic, config)),
          "MySceneAsProtobuf");
    }
    checkSuccess(model.getWorldModelB().connectOtherSceneAsJson(mqttUri("place-a/logical/update", config)),
        "OtherSceneAsJson");
    checkSuccess(model.getWorldModelB().connectNextOperationAsNtaToken(mqttUri(config.forB.topicCommand, config), false),
        "NextOperation");
    // indexed add not supported!!
//    checkSuccess(model.getWorldModelB().connectExecutedOperation(mqttUri(config.forB.topicCommand, config)),
//        "OperationHistory");
    for (Robot robot : model.getWorldModelB().getRobotList()) {
      // self-loop
      checkSuccess(robot.connectOwnedCollaborationZoneNames(mqttUri(config.forB.topicCommand, config)),
          "OwnedCollaborationZoneNames (" + robot.getName() + ")");
      checkSuccess(robot.connectOccupiedCollaborationZoneNames(mqttUri(config.forB.topicCommand, config)),
          "OccupiedCollaborationZoneNames (" + robot.getName() + ")");

      String topicPosition = joinTopics("place-b", robot.getName(), "position");
      checkSuccess(robot.connectCurrentPosition(mqttUri(topicPosition, config)),
          "CurrentPosition (" + robot.getName() + ")");
      checkSuccess(robot.connectMyPosition(mqttUri(topicPosition, config), true),
          "MyPosition (" + robot.getName() + ")");
    }

    // TODO make the actual dependencies
    model.getWorldModelB().addNextOperationWorldModelMySceneDep(model.getWorldModelB());
    model.getWorldModelB().addNextOperationWorldModelOtherSceneDep(model.getWorldModelB());
    for (Robot robot : model.getWorldModelB().getRobotList()) {
      model.getWorldModelB().addNextOperationRobotCurrentPositionDep(robot);
      robot.addMyPositionRobotCurrentPositionDep(robot);
      robot.addMyPositionRobotOccupiedDep(robot);
      robot.addMyPositionRobotOwnedDep(robot);
      robot.addMyPositionMySceneDep(model.getWorldModelB());
    }
  }

  private void checkSuccess(boolean connectSuccess, String target) {
    if (!connectSuccess) {
      logger.warn("Did not connect successfully to {}", target);
    }
  }

  @Override
  protected String getModelInfos(ArtificialRoot artificialRoot, boolean detailed) {
//    Thread t = new Thread(() -> {
      try {
        String filename = artificialRoot.getWorldModelB().dumpAst(builder -> {
          builder.includeChildWhen((parentNode, childNode, contextName) -> {
            switch (contextName) {
              case "Orientation":
              case "Size":
                return false;
              default:
                return true;
            }
          });
          builder.includeTokensWhen((node, tokenName, value) -> {
            switch (tokenName) {
              case "_internal_MySceneAsProtobuf":
              case "_internal_OtherSceneAsJson":
                return false;
              default:
                return true;
            }
          });
          builder.includeAttributeWhen((node, attributeName, isNTA, value) -> {
            if (isNTA) {
              switch (attributeName) {
                case "diffScenes":
                case "diffToOperations":
                case "LogicalScene":
                  return true;
                default:
                  return false;
              }
            } else {
              switch (attributeName) {
                case "realRegion":
                case "computeOperations":
                case "myPosition":
                  return true;
                default:
                  return false;
              }
            }
          });
          builder.includeRelationsWhen((sourceNode, targetNode, roleName) -> {
            if (roleName.startsWith("_internal_") && (roleName.endsWith("Source") || roleName.endsWith("Target"))) {
              return false;
            }
            return !roleName.equals("ContainedInRegion");
          });
          builder.includeNullNodes();
        });
        mainHandler.publish(TOPIC_MODEL_SVG_PATH, filename.getBytes(StandardCharsets.UTF_8));
      } catch (Exception e) {
        logger.catching(e);
      }
//    });
//    t.setDaemon(true);
//    t.start();

    return OldUtilB.getModelInfos(artificialRoot.getWorldModelB(), detailed);
  }

}
