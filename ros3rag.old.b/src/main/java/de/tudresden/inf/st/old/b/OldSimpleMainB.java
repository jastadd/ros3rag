package de.tudresden.inf.st.old.b;

import de.tudresden.inf.st.ceti.Object;
import de.tudresden.inf.st.old.b.ast.*;
import de.tudresden.inf.st.ros3rag.common.Configuration;
import de.tudresden.inf.st.ros3rag.common.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Testing features for placeB.
 *
 * @author rschoene - Initial contribution
 */
public class OldSimpleMainB {
  private static final Logger logger = LogManager.getLogger(OldSimpleMainB.class);

  static class Scenario {
    final String suffix;
    final boolean loadAndChangeScenes;
    final boolean exitAutomatically;
    final String mqttHost;
    final String topicSceneUpdateB;
    final String topicCommand;
    Scenario(String suffix) {
      this(suffix, true, true,
          "localhost", "place-b/scene/update", "place-b/command");
    }
    Scenario(String suffix, boolean loadAndChangeScenes, boolean exitAutomatically,
             String mqttHost, String topicSceneUpdateB, String topicCommand) {
      this.suffix = suffix;
      this.loadAndChangeScenes = loadAndChangeScenes;
      this.exitAutomatically = exitAutomatically;
      this.mqttHost = mqttHost;
      this.topicSceneUpdateB = topicSceneUpdateB;
      this.topicCommand = topicCommand;
    }
  }
  Scenario s2022 = new Scenario("2022");
  Scenario sMini = new Scenario("mini");
  Scenario sPlaceworld = new Scenario("placeworld", false, false,
      "192.168.0.122",
      "/ceti_cell_placeworld/scene/update",
      "/ceti_cell_placeworld/command");
  Scenario sPlaceworldManual = new Scenario("placeworld-manual", false, false,
      "localhost",
      "/ceti_cell_placeworld/scene/update",
      "/ceti_cell_placeworld/command");

  @SuppressWarnings("unused" )
  Scenario[] allScenarios = new Scenario[] { s2022, sMini, sPlaceworld, sPlaceworldManual };

  final Scenario scenario = sMini;

  public static void main(String[] args) throws Exception {
    System.out.println("Running SimpleMainB");
    new OldSimpleMainB().run(args);
  }

  @SuppressWarnings({"unused"})
  private void run(String[] args) throws Exception {
    readModelAndReceiveFromA();
  }

  private void readModelAndReceiveFromA() throws Exception {
    Configuration config = new Configuration();
    config.mqttHost = scenario.mqttHost;
    String filenameInitialSceneB = "src/main/resources/config-scene-b-" + scenario.suffix + ".json";
    config.filenameRegions = "src/main/resources/regions-b-" + scenario.suffix + ".json";
    config.forB = new Configuration.ConfigurationForB();
    config.forB.filenameReachability = "src/main/resources/reachability-b-" + scenario.suffix + ".json";

    Configuration configA = new Configuration();
    String filenameInitialSceneA = "src/main/resources/config-scene-a-" + scenario.suffix + ".json";
    configA.filenameRegions = "src/main/resources/regions-a-" + scenario.suffix + ".json";

    final String topicSceneUpdateB = scenario.topicSceneUpdateB;
    final String topicUpdateFromPlaceA = "update/logical/fromA";
    final String topicCommand = scenario.topicCommand;
    final String topicExit = "place-b/exit";
    final String topicModel = "place-b/model";
    final String topicModelStatus = "place-b/status";

    logger.info("Using scenario {}", scenario.suffix);

    ArtificialRoot root = new ArtificialRoot();

    WorldModelB model = new WorldModelB();
    model.addOtherScene(new LogicalScene());
    root.setWorldModelB(model);

    MqttHandler mqttHandler = new MqttHandler().dontSendWelcomeMessage();
    mqttHandler.setHost(config.mqttHost);
    mqttHandler.waitUntilReady(2, TimeUnit.SECONDS);
    root.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    Scene myScene = new Scene();
    // read initial scene
    Path initialSceneFile = OldUtilB.pathToDirectoryOfPlaceB().resolve(filenameInitialSceneB);
    if (initialSceneFile.toFile().exists()) {
      de.tudresden.inf.st.ceti.Scene initialScene = Util.readScene(initialSceneFile);
      if (scenario.loadAndChangeScenes) {
        myScene = OldUtilB.convert(initialScene);
      } else {
        mqttHandler.newConnection("coordinating/rag-b/command", bytes -> {
          if (new String(bytes).equals("start")) {
            mqttHandler.publish(scenario.topicSceneUpdateB, initialScene.toByteArray());
          }
        });
      }
    }
    model.setMyScene(myScene);

    // read and set regions
    File regionBFile = OldUtilB.pathToDirectoryOfPlaceB().resolve(config.filenameRegions).toFile();
    OldUtilB.setRegions(model, Util.parseRegionConfig(regionBFile));

    // init robots and reachability
    // assumption: robots do not change during runtime, so we have stable connections
    Path path = OldUtilB.pathToDirectoryOfPlaceB().resolve(Paths.get(config.forB.filenameReachability));
    OldReachabilityConfiguration reachability = OldUtilB.readReachability(path.toFile());
    for (OldReachabilityConfiguration.RobotConfiguration robotConfiguration : reachability.robots) {

      Robot robot = OldUtilB.createRobot(robotConfiguration.name);
      model.addRobot(robot);

      JastAddList<CanReachObjectOfInterest> reachabilityList = OldUtilB.convertToReachability(robotConfiguration.reachableLocations);
      robot.setCanReachObjectOfInterestList(reachabilityList);
    }

    Runnable close = () -> {
      logger.info("Exiting ...");
      mqttHandler.close();
      root.ragconnectCloseConnections();
    };
    Runtime.getRuntime().addShutdownHook(new Thread(close));

    model.connectOtherSceneAsJson(Util.mqttUri(topicUpdateFromPlaceA, config));
    model.connectMySceneAsProtobuf(Util.mqttUri(topicSceneUpdateB, config));
    for (Robot robot : model.getRobotList()) {
      robot.connectOwnedCollaborationZoneNames(Util.mqttUri(topicCommand, config));
    }
    model.connectNextOperationAsNtaToken(Util.mqttUri(topicCommand, config), true);

    // prepare exit condition
    final CountDownLatch exitCondition = new CountDownLatch(1);
    mqttHandler.newConnection(topicExit, bytes -> exitCondition.countDown());
    logger.info("Quit via " + topicExit);

    // prepare model status
    mqttHandler.newConnection(topicModel, bytes -> {
      String message = new String(bytes);
      model.dumpAst(builder -> {
        builder.excludeChildren("Orientation", "Size");
        builder.excludeRelations("ContainedInRegion");
        builder.includeNonterminalAttributes("LogicalScene", "diffScenes", "diffToOperations");
        builder.includeAttributes("realRegion", "computeOperations");
        builder.includeNullNodes();
      });
      String content = OldUtilB.getModelInfos(model, message.equals("Start") || message.startsWith("detail"));
      logger.info("WorldModel\n{}", content);
      mqttHandler.publish(topicModelStatus, content.getBytes(StandardCharsets.UTF_8));
    });

    // read scene from A, serialize it and send it via mqtt
    Path sceneAFile = Util.pathToModuleDirectory("ros3rag.placeA").resolve(filenameInitialSceneA);
    File regionAFile = Util.pathToModuleDirectory("ros3rag.placeA").resolve(configA.filenameRegions).toFile();
    de.tudresden.inf.st.ceti.Scene scenePlaceA = Util.readScene(sceneAFile);
    Scene sceneFromPlaceA = OldUtilB.convert(scenePlaceA);
    // need to "wrap" the scene in a world model to access regions. will use one from site-B here for convenience
    WorldModelB tempWorldModel = new WorldModelB();
    tempWorldModel.setMyScene(sceneFromPlaceA);
    OldUtilB.setRegions(tempWorldModel, Util.parseRegionConfig(regionAFile));
    LogicalScene logicalSceneFromPlaceA = sceneFromPlaceA.getLogicalScene();
    byte[] bytesToSend = _ragconnect__apply__TreeDefaultLogicalSceneToBytesMapping(logicalSceneFromPlaceA);

    if (scenario.loadAndChangeScenes && initialSceneFile.toFile().exists()) {
      de.tudresden.inf.st.ceti.Scene initialScene = Util.readScene(initialSceneFile);

      describedWait(1, "send new logical scene" );
      mqttHandler.publish(topicUpdateFromPlaceA, bytesToSend);

      describedWait(2, "print model status" );
      mqttHandler.publish(topicModel, "detailed".getBytes(StandardCharsets.UTF_8));

      // set object O1 to position of P-E in sceneB and publish it
      describedWait(3, "send updated sceneB (P-E)");
      de.tudresden.inf.st.ceti.Scene scene = updatePositionOfObject(initialScene, "O1", myScene.resolveObjectOfInterest("P-E").getPosition());
      mqttHandler.publish(topicSceneUpdateB, scene.toByteArray());

      describedWait(4, "set R1 to not busy");
      scene = updateNotBusyOfRobot(scene, "R1");

      describedWait(5, "print model status");
      mqttHandler.publish(topicModel, "detailed".getBytes(StandardCharsets.UTF_8));

      // set object O1 to position of P2.2 in sceneB and publish it
      describedWait(6, "send updated sceneB (P2.2)");
      scene = updatePositionOfObject(scene, "O1", myScene.resolveObjectOfInterest("P2.2").getPosition());
      mqttHandler.publish(topicSceneUpdateB, scene.toByteArray());

      describedWait(7, "print model status");
      mqttHandler.publish(topicModel, "detailed".getBytes(StandardCharsets.UTF_8));

      describedWait(8, "set R2 to not busy");
      scene = updateNotBusyOfRobot(scene, "R2");
      mqttHandler.publish(topicSceneUpdateB, scene.toByteArray());

      describedWait(9, "print model status");
      mqttHandler.publish(topicModel, "detailed".getBytes(StandardCharsets.UTF_8));
    } else {
      mqttHandler.newConnection("coordinating/rag-a/command", bytes -> {
        if (new String(bytes).equals("start")) {
          mqttHandler.publish(topicUpdateFromPlaceA, bytesToSend);
        }
      });
    }

    if (scenario.exitAutomatically) {
      TimeUnit.SECONDS.sleep(5);
      exitCondition.countDown();
    }

    exitCondition.await();
  }

  static void describedWait(int stepNr, String description) throws InterruptedException {
    logger.info("({}) Wait 2 secs, then {}", stepNr, description);
    TimeUnit.SECONDS.sleep(2);
    logger.info(description);
  }

  static de.tudresden.inf.st.ceti.Scene updatePositionOfObject(
      de.tudresden.inf.st.ceti.Scene scene,
      @SuppressWarnings("SameParameterValue" ) String objectName,
      Position newPosition) {
    return OldUtilB.updateObject(scene, objectName, obj -> {
      Object.Builder builder = obj.toBuilder();
      builder.getPosBuilder()
          .setX(newPosition.getX())
          .setY(newPosition.getY())
          .setZ(newPosition.getZ());
      return builder.build();
    });
  }

  static de.tudresden.inf.st.ceti.Scene updateNotBusyOfRobot(
      de.tudresden.inf.st.ceti.Scene scene,
      String objectName) {
    return OldUtilB.updateObject(scene, objectName, obj -> obj.toBuilder().setState(Object.State.STATE_MOVING).build());
  }


  // copied from WorldModelA
  protected static byte[] _ragconnect__apply__TreeDefaultLogicalSceneToBytesMapping(LogicalScene input) throws Exception {
    java.io.ByteArrayOutputStream outputStream = new java.io.ByteArrayOutputStream();
    com.fasterxml.jackson.core.JsonFactory factory = new com.fasterxml.jackson.core.JsonFactory();
    com.fasterxml.jackson.core.JsonGenerator generator = factory.createGenerator(outputStream, com.fasterxml.jackson.core.JsonEncoding.UTF8);
    input.serialize(generator);
    generator.flush();
    return outputStream.toString().getBytes();
  }

  @SuppressWarnings("unused")
  private void printReachability(WorldModelB model) {
    System.out.println("ModelInfos:");
    System.out.println(OldUtilB.getModelInfos(model, true));

    System.out.println("Reachability:");
    model.getRobotList().forEach(r -> System.out.println(r.getName() + ": " + r.reachableObjects().stream().map(ObjectOfInterest::getName).collect(Collectors.joining(", "))));

    System.out.println("ReachabilityGraph:");
    System.out.println(model.toReachabilityGraph().prettyPrint());
  }

  @SuppressWarnings("unused")
  private void printShortestPath(WorldModelB model, String source, String target) {
    LogicalRegion sourceLocation = model.getMyScene().getLogicalScene()
        .resolveLogicalObjectOfInterest(source).asLogicalRegion();
    LogicalRegion targetLocation = model.getMyScene().getLogicalScene()
        .resolveLogicalObjectOfInterest(target).asLogicalRegion();

    List<Edge> bfs = sourceLocation.correspondingVertex().orElseThrow().BFS(targetLocation.correspondingVertex().orElseThrow());

    System.out.println("Shortest path from " + sourceLocation.getName() + " to " + targetLocation.getName() + ": " + bfs);
  }

  @SuppressWarnings("unused")
  private void testBFS() {
    /*
      A   B
      | \ |
      C - D
     */
    Graph g = new Graph();
    Vertex a = makeVertex("a");
    Vertex b = makeVertex("b");
    Vertex c = makeVertex("c");
    Vertex d = makeVertex("d");
    Edge ac = makeEdge(a, c);
    Edge ad = makeEdge(a, d);
    Edge cd = makeEdge(c, d);
    Edge db = makeEdge(d, b);
    g.addVertex(a);
    g.addVertex(b);
    g.addVertex(c);
    g.addVertex(d);
    g.addEdge(ac);
    g.addEdge(ad);
    g.addEdge(cd);
    g.addEdge(db);

    System.out.println(a.BFS(b));
  }

  private Vertex makeVertex(String name) {
    return new Vertex() {
      @Override
      public String toString() {
        return name;
      }
    };
  }

  private Edge makeEdge(Vertex from, Vertex to) {
    Edge result = new Edge();
    result.setFrom(from);
    result.setTo(to);
    return result;
  }

  @SuppressWarnings("unused")
  private void testDistance() throws Exception {
    de.tudresden.inf.st.ceti.Scene scene = Util.readScene(
        OldUtilB.pathToDirectoryOfPlaceB().resolve("src/main/resources/config-scene-b.json")
    );

    Scene myScene = OldUtilB.convert(scene);
    float arm1X = 0f;
    float arm1Y = 0f;
    float arm1Z = 0.75f;

    float arm2X = 1f;
    float arm2Y = 0f;
    float arm2Z = 0.75f;

    for (var location : myScene.getDropOffLocationList()) {
      check(arm1X, arm1Y, arm1Z, location.getPosition(), "arm1", location.getName());
      check(arm2X, arm2Y, arm2Z, location.getPosition(), "arm2", location.getName());
    }
  }

  private void check(double x, double y, double z, Position position, String robotName, String locationName) {
    double dx = x - position.getX();
    double dy = y - position.getY();
    double dz = z - position.getZ();

    if (Math.sqrt(dx*dx + dy*dy) < 0.05) {
      System.out.println(robotName + " too close to " + locationName);
    }
    if (Math.sqrt(dx*dx + dy*dy + dz*dz) > 0.75) {
      System.out.println(robotName + " too far away from " + locationName);
    }
  }

  @SuppressWarnings("unused")
  private void testBuildModelB() {
    WorldModelB model = new WorldModelB();

    // robots
    model.addRobot(OldUtilB.createRobot("Alice", "placeAlfa", "placeBeta"));
    model.addRobot(OldUtilB.createRobot("Bob", "placeBeta", "placeGamma"));

    // myScene
    var myScene = new Scene();
    myScene.addMovableObject(MovableObject.of("obj1", Position.of(0, 0, 0)));
    myScene.addDropOffLocation(DropOffLocation.of("placeAlfa",
        Position.of(0, 0, 0),
        Orientation.of(0, 0, 0, 1),
        Size.of(1, 1, 1)));
    myScene.addDropOffLocation(DropOffLocation.of("placeBeta",
        Position.of(1, 1, 1),
        Orientation.of(0, 0, 0, 1),
        Size.of(1, 1, 1)));
    myScene.addDropOffLocation(DropOffLocation.of("placeGamma",
        Position.of(3, 3, 3),
        Orientation.of(0, 0, 0, 1),
        Size.of(2, 2, 2)));
    logger.info("obj1 at alfa: {}",
        myScene.getMovableObject(0).isLocatedAt(myScene.getDropOffLocation(0)));
    model.setMyScene(myScene);

    // otherScene
    var otherScene = new LogicalScene();
    LogicalMovableObject otherObj1 = new LogicalMovableObject().setName("obj1");
    otherScene.addLogicalMovableObject(otherObj1);
    otherScene.addLogicalRegion(new LogicalRegion().setName("placeAlfa"));
    otherScene.addLogicalRegion(new LogicalRegion().setName("placeBeta"));
    LogicalRegion otherGamma = new LogicalRegion().setName("placeGamma");
    otherGamma.addContainedObject(otherObj1);
    otherScene.addLogicalRegion(otherGamma);
    model.addOtherScene(otherScene);

    // printing and testing
    printModelInfos(model);

    // now simulate, that Alice has moved obj1 to placeBeta
    myScene.getMovableObject(0).setPosition(Position.of(1, 1, 1));
    logger.warn("Moving obj1 manually to beta");

    printModelInfos(model);

    // now simulate, that Bob has moved obj1 to placeGamma
    myScene.getMovableObject(0).setPosition(Position.of(3, 3, 3));
    logger.warn("Moving obj1 manually to gamma");

    // expect error-operation
    printModelInfos(model);
  }

  private void printModelInfos(WorldModelB model) {
    logger.info(OldUtilB.getModelInfos(model));
  }
}
