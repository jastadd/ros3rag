package de.tudresden.inf.st.old.b;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.ceti.Object;
import de.tudresden.inf.st.old.b.ast.*;
import de.tudresden.inf.st.ros3rag.common.RegionConfiguration;
import de.tudresden.inf.st.ros3rag.common.RegionConfiguration.RegionDefinition;
import de.tudresden.inf.st.ros3rag.common.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;

/**
 * Static utility methods used only for place B.
 *
 * @author rschoene - Initial contribution
 */
public class OldUtilB {
  private static final Logger logger = LogManager.getLogger(OldUtilB.class);

  static OldReachabilityConfiguration readReachability(File reachabilityFile) throws IOException {
    logger.info("Using reachability config file: {}", reachabilityFile.getAbsolutePath());
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(reachabilityFile, OldReachabilityConfiguration.class);
  }

  static Path pathToDirectoryOfPlaceB() {
    return Util.pathToModuleDirectory("ros3rag.old.b");
  }

  static Robot createRobot(String name, String... canReach) {
    Robot result = new Robot().setName(name).setCurrentPosition("unknown");

    for (String canReachName : canReach) {
      result.addCanReachObjectOfInterest(new CanReachObjectOfInterest(canReachName));
    }
    return result;
  }

  private static String resolveObjName(WorldModelB model, String objName) {
    if (!model.hasMyScene()) {
      return "\"" + objName + "\"";
    }
    try {
      return model.getMyScene().resolveObjectOfInterest(objName).getName();
    } catch (NullPointerException ignore) {
      return "+" + objName + "(not resolved)+";
    }
  }

  static String getModelInfos(WorldModelB model) {
    return getModelInfos(model, false);
  }

  static String getModelInfos(WorldModelB model, boolean detailed) {
    StringBuilder sb = new StringBuilder();
    sb.append("myRobots: ")
        .append(model.getRobotList().prettyPrint(
            robot -> robot.getName()
                + "(" + (robot.isBusy() ? "busy" : "free")
                + ", canReach: "
                + robot.getCanReachObjectOfInterestList().prettyPrint(
                    canReachObj -> resolveObjName(model, canReachObj.getObjectName()))
                + ")"))
        .append("\n");
    if (detailed) {
      // also include "normal" scene
      sb.append("myScene:");
      if (model.hasMyScene()) {
        sb.append("\n").append(model.getMyScene().prettyPrint());
      } else {
        sb.append(" (unset)\n");
      }
    }
    sb.append("myLogicalScene:");
    if (model.hasMyScene()) {
      sb.append("\n").append(model.getMyScene().getLogicalScene().prettyPrint());
    } else {
      sb.append(" (unset)\n");
    }
    sb.append("otherScene:");
    if (model.hasOtherScene()) {
      sb.append("\n").append(model.mergedOtherScene().prettyPrint());
    } else {
      sb.append(" (unset)\n");
    }
    sb.append("Diff: ").append(model.diffScenes().prettyPrint(Difference::prettyPrint)).append("\n");
    if (detailed) {
      sb.append("Graph: ").append(model.toReachabilityGraph().prettyPrint()).append("\n");
    }
    sb.append("Operations: ").append(model.diffToOperations().prettyPrint(Operation::prettyPrint)).append("\n");
    sb.append("Next operation: ").append(model.getNextOperation().prettyPrint()).append("\n");
    sb.append("Operation History: ").append(model.getExecutedOperations().prettyPrint(Operation::prettyPrint)).append("\n");
//    String summary = model.ragconnectEvaluationCounterSummary();
//    sb.append("EvaluationCounter:\n").append(summary);
//    sb.append("% skipped: ").append(percentageSkipped(summary)).append("\n");
    return sb.toString();
  }

  static double percentageSkipped(String summary) {
    int sumCall = 0;
    int sumSkipped = 0;
    try {
      for (String line : summary.split("\n")) {
        String[] tokens = line.split(",");
        if (tokens.length == 0 || tokens[0].equals("parentTypeName")) {
          continue;
        }
        sumCall += Integer.parseInt(tokens[4]);
        sumSkipped += Integer.parseInt(tokens[6]);
      }
    } catch (NumberFormatException e) {
      logger.catching(e);
    }
    if (sumCall == 0) {
      return 0;
    }
    return sumSkipped * 100.0 / sumCall;
  }

  static Scene convert(de.tudresden.inf.st.ceti.Scene scene) throws Exception {
    return new ExposingASTNode().exposed_apply_ConvertScene(scene);
  }

  static JastAddList<CanReachObjectOfInterest> convertToReachability(List<String> reachableLocations) {
    JastAddList<CanReachObjectOfInterest> result = new JastAddList<>();
    reachableLocations.forEach(location -> result.addChild(new CanReachObjectOfInterest(location)));
    return result;
  }

  static void setRegions(WorldModelB model, RegionConfiguration config) {
    JastAddList<Region> result = new JastAddList<>();
    for (RegionDefinition def : config.regions) {
      Region region = new Region();
      region.setName(def.name);
      region.setLocationNames(String.join(",", def.positions));
      result.add(region);
    }
    model.setRegionList(result);
  }

  static de.tudresden.inf.st.ceti.Scene updateObject(
      de.tudresden.inf.st.ceti.Scene scene,
      String objectName,
      Function<Object, Object> change) {
    List<Object> objectsList = scene.getObjectsList();
    Object newObj = null;
    int index, objectsListSize;
    for (index = 0, objectsListSize = objectsList.size(); index < objectsListSize; index++) {
      Object obj = objectsList.get(index);
      if (obj.getId().equals(objectName)) {
        newObj = change.apply(obj);
        break;
      }
    }
    if (newObj == null) {
      logger.error("Did not find object {}!", objectName);
    } else {
      scene = scene.toBuilder().setObjects(index, newObj).build();
      logger.info("Update {} in scene to:\n {}", objectName, newObj);
    }
    return scene;
  }

  @SuppressWarnings("rawtypes")
  static class ExposingASTNode extends ASTNode {
    public Scene exposed_apply_ConvertScene(de.tudresden.inf.st.ceti.Scene pbScene) throws Exception {
      return _apply_ConvertScene(pbScene);
    }
//    public CanReachObjectOfInterestWrapper exposed_apply_ConvertReachability(de.tudresden.inf.st.ceti.Reachability r) throws Exception {
//      return _ragconnect__apply_ConvertReachability(r);
//    }
  }

}
