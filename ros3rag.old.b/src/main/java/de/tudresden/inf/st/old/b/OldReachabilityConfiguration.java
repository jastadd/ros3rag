package de.tudresden.inf.st.old.b;

import java.util.List;

/**
 * Data class for reachability information.
 *
 * @author rschoene - Initial contribution
 */
public class OldReachabilityConfiguration {
  public List<RobotConfiguration> robots;

  public static class RobotConfiguration {
    public String name;
    public List<String> reachableLocations;
  }
}
