package de.tudresden.inf.st.placeB;

import de.tudresden.inf.st.placeB.ast.*;
import org.junit.jupiter.api.Test;

import static de.tudresden.inf.st.placeB.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing computing differences.
 *
 * @author rschoene - Initial contribution
 */
public class TestDifference {

  @Test
  public void testDifferenceObjectAtWrongPlaceNoPreviousLocation() {
    // myScene: object (x) not contained in any location
    // targetScene: object (x) in location (red)
    WorldModelB model = newModel();
    addMyObject(model, "x", new Position(4, 4, 4), noRotation(), new Size(0.5f, 0.5f, 0.5f));
    addMyLocation(model, "red", new Position(0, 0, 0), noRotation(), new Size(1, 1, 1));

    addOtherObjects(model, "x");
    addOtherRegions(model, "red");
    addContainedObjects(model, "red", "x");

    LogicalScene myLogicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = myLogicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    assertFalse(x.hasLocatedAt());

    JastAddList<Difference> diffs = model.diffScenes();
    assertNotNull(diffs);
    assertEquals(1, diffs.getNumChild());
    Difference diff = diffs.getChild(0);
    assertThat(diff).isInstanceOf(DifferenceObjectAtWrongPlace.class);
    DifferenceObjectAtWrongPlace wrongPlace = (DifferenceObjectAtWrongPlace) diff;
    assertEquals("x", wrongPlace.getObject().getName());
    assertNull(wrongPlace.getPreviousRegion());
    assertEquals("red", wrongPlace.getNewRegion().getName());
  }

  @Test
  public void testDifferenceObjectAtWrongPlaceOtherPreviousLocation() {
    // myScene: object (x) contained in another location (blue)
    // targetScene: object (x) in location (red)
    WorldModelB model = newModel();
    addMyObject(model, "x", new Position(0.5f, 0.5f, 0.5f), noRotation(), new Size(0.5f, 0.5f, 0.5f));
    addMyLocation(model, "blue", new Position(0, 0, 0), noRotation(), new Size(1, 1, 1));

    addOtherObjects(model, "x");
    addOtherRegions(model, "red");
    addContainedObjects(model, "red", "x");

    LogicalScene myLogicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = myLogicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    assertEquals("P-blue", x.getNameOfMyLocation());

    JastAddList<Difference> diffs = model.diffScenes();
    assertNotNull(diffs);
    assertEquals(1, diffs.getNumChild());
    Difference diff = diffs.getChild(0);
    assertThat(diff).isInstanceOf(DifferenceObjectAtWrongPlace.class);
    DifferenceObjectAtWrongPlace wrongPlace = (DifferenceObjectAtWrongPlace) diff;
    assertEquals("x", wrongPlace.getObject().getName());
    assertEquals("blue", wrongPlace.getPreviousRegion().getName());
    assertEquals("red", wrongPlace.getNewRegion().getName());
  }

  @Test
  public void testDifferenceObjectMisplaced() {
    // myScene: object (x) contained in a location (red)
    // targetScene: object (x) not contained in any location
    WorldModelB model = newModel();
    addMyObject(model, "x", new Position(0.5f, 0.5f, 0.5f), noRotation(), new Size(0.5f, 0.5f, 0.5f));
    addMyLocation(model, "red", new Position(0, 0, 0), noRotation(), new Size(1, 1, 1));

    addOtherObjects(model, "x");
    addOtherRegions(model, "red");

    LogicalScene myLogicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = myLogicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    assertEquals("P-red", x.getNameOfMyLocation());

    JastAddList<Difference> diffs = model.diffScenes();
    assertNotNull(diffs);
    assertEquals(1, diffs.getNumChild());
    Difference diff = diffs.getChild(0);
    assertThat(diff).isInstanceOf(DifferenceObjectMisplaced.class);
    DifferenceObjectMisplaced misplaced = (DifferenceObjectMisplaced) diff;
    assertEquals("x", misplaced.getObject().getName());
    assertEquals("red", misplaced.getPreviousRegion().getName());
  }

  @Test
  public void testNoDifferenceNoContainment() {
    // myScene: object (x) not contained in any location
    // targetScene: object (x) not contained in any location
    WorldModelB model = newModel();
    addMyObject(model, "x", new Position(8, 8, 8), noRotation(), new Size(0.5f, 0.5f, 0.5f));
    addMyLocation(model, "red", new Position(0, 0, 0), noRotation(), new Size(1, 1, 1));

    addOtherObjects(model, "x");
    addOtherRegions(model, "red");

    LogicalScene myLogicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = myLogicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    assertFalse(x.hasLocatedAt());

    JastAddList<Difference> diffs = model.diffScenes();
    assertNotNull(diffs);
    assertThat(diffs).isEmpty();
  }

  @Test
  public void testNoDifferenceSameLocation() {
    // myScene: object (x) contained in same location (red)
    // targetScene: object (x) contained in a location (red)
    WorldModelB model = newModel();
    addMyObject(model, "x", new Position(0.5f, 0.5f, 0.5f), noRotation(), new Size(0.5f, 0.5f, 0.5f));
    addMyLocation(model, "red", new Position(0, 0, 0), noRotation(), new Size(1, 1, 1));

    addOtherObjects(model, "x");
    addOtherRegions(model, "red");
    addContainedObjects(model, "red", "x");

    LogicalScene myLogicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = myLogicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    assertEquals("P-red", x.getNameOfMyLocation());

    JastAddList<Difference> diffs = model.diffScenes();
    assertNotNull(diffs);
    assertThat(diffs).isEmpty();
  }

  @Test
  public void testDifferenceNewObject() {
    // myScene: object (x) not known
    // targetScene: object (x) exists, not contained in any location
    WorldModelB model = newModel();
    addMyLocation(model, "red", new Position(0, 0, 0), noRotation(), new Size(1, 1, 1));

    addOtherObjects(model, "x");
    addOtherRegions(model, "red");

    LogicalScene myLogicalScene = model.getMyScene().getLogicalScene();
    assertNull(myLogicalScene.resolveLogicalObjectOfInterest("x"));

    JastAddList<Difference> diffs = model.diffScenes();
    assertNotNull(diffs);
    assertEquals(1, diffs.getNumChild());
    Difference diff = diffs.getChild(0);
    assertThat(diff).isInstanceOf(DifferenceNewObject.class);
    DifferenceNewObject diffNewObject = (DifferenceNewObject) diff;
    assertEquals("x", diffNewObject.getObject().getName());
  }

  @Test
  public void testMultipleDifferenceObjectAtWrongPlace() {
    // myScene: objects (x, y) contained in other locations (blue, green)
    // targetScene: objects (x, y) in location (red)
    WorldModelB model = newModel();
    addMyObject(model, "x", new Position(0.5f, 0.5f, 0.5f), noRotation(), new Size(0.5f, 0.5f, 0.5f));
    addMyObject(model, "y", new Position(2.5f, 2.5f, 2.5f), noRotation(), new Size(0.5f, 0.5f, 0.5f));
    addMyLocation(model, "blue", new Position(0, 0, 0), noRotation(), new Size(1, 1, 1));
    addMyLocation(model, "green", new Position(2, 2, 2), noRotation(), new Size(1, 1, 1));

    addOtherObjects(model, "x", "y");
    addOtherRegions(model, "red");
    addContainedObjects(model, "red", "x", "y");

    LogicalScene myLogicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = myLogicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    LogicalMovableObject y = myLogicalScene.resolveLogicalObjectOfInterest("y").asLogicalMovableObject();
    assertEquals("P-blue", x.getNameOfMyLocation());
    assertEquals("P-green", y.getNameOfMyLocation());

    JastAddList<Difference> diffs = model.diffScenes();
    assertNotNull(diffs);
    assertEquals(2, diffs.getNumChild());
    assertThat(diffs).allMatch(d -> d instanceof DifferenceObjectAtWrongPlace);
    assertThat(diffs).map(diff -> (DifferenceObjectAtWrongPlace) diff)
        .extracting(d -> d.getObject().getName(), d -> d.getPreviousRegion().getName(), d -> d.getNewRegion().getName())
        .containsOnly(tuple("x", "blue", "red"),
            tuple("y", "green", "red"));
  }
}
