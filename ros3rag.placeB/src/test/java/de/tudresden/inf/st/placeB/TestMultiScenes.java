package de.tudresden.inf.st.placeB;

import de.tudresden.inf.st.placeB.ast.LogicalRegion;
import de.tudresden.inf.st.placeB.ast.LogicalMovableObject;
import de.tudresden.inf.st.placeB.ast.LogicalScene;
import de.tudresden.inf.st.placeB.ast.WorldModelB;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Collections;

import static de.tudresden.inf.st.placeB.TestUtils.newModel;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Testing merging of multiple (other) scenes.
 *
 * @author rschoene - Initial contribution
 */
public class TestMultiScenes {

  @Test
  public void noOtherScenes() {
    WorldModelB model = newModel();

    LogicalScene actual = model.mergedOtherScene();

    assertThat(actual.getLogicalRegionList()).isEmpty();
    assertThat(actual.getLogicalMovableObjectList()).isEmpty();
  }

  @ParameterizedTest(name = "twoScenesContains [scene1 = {argumentsWithNames}]")
  @ValueSource(booleans = {true, false})
  public void oneSceneNoContains(boolean scene1) {
    WorldModelB model = newModel();
    TestUtils.addOtherRegions(model.getOtherScene(scene1 ? 0 : 1), "red");
    TestUtils.addOtherObjects(model.getOtherScene(scene1 ? 0 : 1), "a");

    LogicalScene actual = model.mergedOtherScene();

    assertThat(actual.getLogicalRegionList())
        .extracting("Name").containsOnly("red");
    assertThat(actual.getLogicalMovableObjectList())
        .extracting("Name").containsOnly("a");
  }

  @Test
  public void twoScenesNoContains() {
    WorldModelB model = newModel();
    TestUtils.addOtherRegions(model, "red");
    TestUtils.addOtherObjects(model, "a");
    TestUtils.addOtherRegions(model.getOtherScene(1), "blue");
    TestUtils.addOtherObjects(model.getOtherScene(1), "b");

    LogicalScene actual = model.mergedOtherScene();

    assertThat(actual.getLogicalRegionList())
        .size().isEqualTo(2);
    assertThat(actual.getLogicalRegionList())
        .doesNotContainAnyElementsOf(model.getOtherScene(0).getLogicalRegionList())
        .doesNotContainAnyElementsOf(model.getOtherScene(1).getLogicalRegionList())
        .extracting("Name").containsOnly("red", "blue");
    assertThat(actual.getLogicalRegionList())
        .extracting("ContainedObjects").containsOnly(Collections.emptyList());

    assertThat(actual.getLogicalMovableObjectList())
        .size().isEqualTo(2);
    assertThat(actual.getLogicalMovableObjectList())
        .doesNotContainAnyElementsOf(model.getOtherScene(0).getLogicalMovableObjectList())
        .doesNotContainAnyElementsOf(model.getOtherScene(1).getLogicalMovableObjectList())
        .extracting("Name").containsOnly("a", "b");
    assertThat(actual.getLogicalMovableObjectList())
        .extracting("LocatedAtList").containsExactly(Collections.emptyList(), Collections.emptyList());
  }

  @ParameterizedTest(name = "twoScenesContains [scene1 = {argumentsWithNames}]")
  @ValueSource(booleans = {true, false})
  public void twoScenesContains1(boolean scene1) {
    WorldModelB model = newModel();
    TestUtils.addOtherRegions(model, "red", "blue");
    TestUtils.addOtherObjects(model, "a", "b");
    model.getOtherScene(1).addLogicalRegion(new LogicalRegion().setName("red"));
    model.getOtherScene(1).addLogicalMovableObject(new LogicalMovableObject().setName("a"));
    TestUtils.addContainedObjects(model.getOtherScene(scene1 ? 0 : 1),
        "red", "a");

    LogicalScene actual = model.mergedOtherScene();

    assertThat(actual.getLogicalRegionList())
        .size().isEqualTo(2);
    assertThat(actual.getLogicalRegionList())
        .doesNotContainAnyElementsOf(model.getOtherScene(0).getLogicalRegionList())
        .doesNotContainAnyElementsOf(model.getOtherScene(1).getLogicalRegionList())
        .extracting("Name").containsExactly("red", "blue");
    LogicalRegion red = actual.resolveLogicalObjectOfInterest("red").asLogicalRegion();
    assertThat(red.getContainedObjectList())
        .extracting("Name").containsOnly("a");

    assertThat(actual.getLogicalMovableObjectList())
        .size().isEqualTo(2);
    assertThat(actual.getLogicalMovableObjectList())
        .doesNotContainAnyElementsOf(model.getOtherScene(0).getLogicalMovableObjectList())
        .extracting("Name").containsOnly("a", "b");
    LogicalMovableObject a = actual.resolveLogicalObjectOfInterest("a").asLogicalMovableObject();
    assertThat(a.getLocatedAtList())
        .extracting("Name").containsExactly("red");
  }

  @Test
  public void twoScenesRedContainsATwice() {
    WorldModelB model = newModel();
    TestUtils.addOtherRegions(model, "red", "blue");
    TestUtils.addOtherObjects(model, "a", "b");
    TestUtils.addContainedObjects(model, "red", "a");

    TestUtils.addOtherRegions(model.getOtherScene(1), "red");
    TestUtils.addOtherObjects(model.getOtherScene(1), "a", "b");
    TestUtils.addContainedObjects(model.getOtherScene(1), "red", "a");

    LogicalScene actual = model.mergedOtherScene();

    assertThat(actual.getLogicalRegionList())
        .size().isEqualTo(2);
    assertThat(actual.getLogicalRegionList())
        .doesNotContainAnyElementsOf(model.getOtherScene(0).getLogicalRegionList())
        .doesNotContainAnyElementsOf(model.getOtherScene(1).getLogicalRegionList())
        .extracting("Name").containsExactly("red", "blue");
    LogicalRegion red = actual.resolveLogicalObjectOfInterest("red").asLogicalRegion();
    assertThat(red.getContainedObjectList())
        .extracting("Name").containsOnly("a");

    assertThat(actual.getLogicalMovableObjectList())
        .size().isEqualTo(2);
    assertThat(actual.getLogicalMovableObjectList())
        .doesNotContainAnyElementsOf(model.getOtherScene(0).getLogicalMovableObjectList())
        .extracting("Name").containsOnly("a", "b");
    LogicalMovableObject a = actual.resolveLogicalObjectOfInterest("a").asLogicalMovableObject();
    assertThat(a.getLocatedAtList())
        .extracting("Name").containsExactly("red");
  }
}
