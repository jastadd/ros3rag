package de.tudresden.inf.st.placeB;

import de.tudresden.inf.st.placeB.ast.*;

/**
 * Utility methods for testing.
 *
 * @author rschoene - Initial contribution
 */
public class TestUtils {
  public static WorldModelB newModel() {
    WorldModelB model = new WorldModelB();
    Scene scene = new Scene();
    model.setMyScene(scene);
    model.addOtherScene(new LogicalScene());
    model.addOtherScene(new LogicalScene());
    return model;
  }

  public static Orientation noRotation() {
    return new Orientation().setW(1);
  }

  public static void addMyObjects(WorldModelB model, String... names) {
    for (String name : names) {
      int index = model.getMyScene().getNumMovableObject();
      addMyObject(model, name, new Position(-index, -index, -index), new Orientation(), new Size(1, 1, 1));
    }
  }

  public static void addMyObject(WorldModelB model, String name, Position position, Orientation orientation, Size size) {
    MovableObject obj = new MovableObject()
        .setName(name)
        .setPosition(position)
        .setOrientation(orientation)
        .setSize(size);
    model.getMyScene().addMovableObject(obj);
  }

  public static void addMyLocationsAndRegions(WorldModelB model, String... names) {
    for (String name : names) {
      int index = model.getMyScene().getNumDropOffLocation();
      addMyLocation(model, name, new Position(+index, +index, +index), new Orientation(), new Size(1, 1, 1));
    }
  }

  public static void addMyLocation(WorldModelB model, String name, Position position, Orientation orientation, Size size) {
    String locName = "P-" + name;
    DropOffLocation obj = new DropOffLocation()
        .setName(locName)
        .setPosition(position)
        .setOrientation(orientation)
        .setSize(size);
    model.getMyScene().addDropOffLocation(obj);
    model.addRegion(new Region().setName(name).setLocationNames(locName));
  }

  public static void addRobots(WorldModelB model, String... names) {
    for (String name : names) {
      Robot obj = new Robot().setName(name);
      model.addRobot(obj);
    }
  }

  public static void addReachability(WorldModelB model, String robotName, String... reachableObjects) {
    Robot robot = model.findRobot(robotName).orElseThrow();
    for (String reachableObjectName : reachableObjects) {
      robot.addCanReachObjectOfInterest(new CanReachObjectOfInterest().setObjectName(reachableObjectName));
    }
  }

  public static void addOtherObjects(WorldModelB model, String... names) {
    addOtherObjects(model.getOtherScene(0), names);
  }

  public static void addOtherObjects(LogicalScene scene, String... names) {
    for (String name : names) {
      scene.addLogicalMovableObject(new LogicalMovableObject().setName(name));
    }
  }

  public static void addOtherRegions(WorldModelB model, String... names) {
    addOtherRegions(model.getOtherScene(0), names);
  }

  public static void addOtherRegions(LogicalScene scene, String... names) {
    for (String name : names) {
      scene.addLogicalRegion(new LogicalRegion().setName(name));
    }
  }

  public static void addContainedObjects(WorldModelB model, String regionName, String... objectNames) {
    addContainedObjects(model.getOtherScene(0), regionName, objectNames);
  }

  public static void addContainedObjects(LogicalScene scene, String regionName, String... objectNames) {
    LogicalRegion region = scene.resolveLogicalObjectOfInterest(regionName).asLogicalRegion();
    for (String objectName : objectNames) {
      region.addContainedObject(scene.resolveLogicalObjectOfInterest(objectName).asLogicalMovableObject());
    }
  }

}
