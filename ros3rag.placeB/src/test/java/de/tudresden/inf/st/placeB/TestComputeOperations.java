package de.tudresden.inf.st.placeB;

import de.tudresden.inf.st.placeB.ast.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static de.tudresden.inf.st.placeB.TestUtils.*;
import static de.tudresden.inf.st.placeB.TestUtils.addMyObjects;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testing computing operations from differences.
 *
 * @author rschoene - Initial contribution
 */
@Disabled("LogicalScene does not have enough information for located at, and new commands are not handled yet")
public class TestComputeOperations {

  @ParameterizedTest(name = "testNoMovePossible [emptyPreviousLocation = {argumentsWithNames}]")
  @ValueSource(booleans = {true, false})
  public void testNoMovePossible(boolean emptyPreviousLocation) {
    WorldModelB model = newModel();
    addMyObjects(model, "x");
    addMyLocationsAndRegions(model, "red", "blue", "green");
    addRobots(model, "arm1", "arm2");
    addReachability(model, "arm1", "P-red", "P-blue", "x");
    addReachability(model, "arm2", "P-blue");

    LogicalScene logicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = logicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    LogicalRegion red = logicalScene.resolveLogicalObjectOfInterest("red").asLogicalRegion();
    LogicalRegion green = logicalScene.resolveLogicalObjectOfInterest("green").asLogicalRegion();
    DifferenceObjectAtWrongPlace diff = createDifferenceObjectAtWrongPlace(x, emptyPreviousLocation ? null : red, green);

    List<Operation> operations = diff.computeOperations();
    assertThat(operations).isNotEmpty();
    assertThat(operations).hasSize(1);
    Operation op = operations.get(0);
    assertTrue(op.isErrorOperation());
    assertEquals(emptyPreviousLocation ?
        "+Error: No sequence of operations to move x to green+" :
        "+Error: No sequence of operations to move x from red to green+", op.prettyPrint());
  }

  @ParameterizedTest(name = "testDirectMove [emptyPreviousLocation = {argumentsWithNames}]")
  @ValueSource(booleans = {true, false})
  public void testDirectMove(boolean emptyPreviousLocation) {
    WorldModelB model = newModel();
    addMyObjects(model, "x");
    addMyLocationsAndRegions(model, "red", "blue", "green");
    addRobots(model, "arm1", "arm2");
    addReachability(model, "arm1", "red", "blue", "x");
    addReachability(model, "arm2", "blue");

    LogicalScene logicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = logicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    LogicalRegion red = logicalScene.resolveLogicalObjectOfInterest("red").asLogicalRegion();
    LogicalRegion blue = logicalScene.resolveLogicalObjectOfInterest("blue").asLogicalRegion();
    Robot arm1 = model.findRobot("arm1").orElseThrow();

    // object "x" shall be place at red or blue
    DifferenceObjectAtWrongPlace diff = createDifferenceObjectAtWrongPlace(x, emptyPreviousLocation ? null : red, blue);
    List<Operation> operations = diff.computeOperations();

    assertThat(operations).isNotEmpty();
    assertThat(operations).hasSize(1);
    Operation op = operations.get(0);
    assertThat(op).isInstanceOf(PickAndPlace.class);
    PickAndPlace pickAndPlace = (PickAndPlace) op;
    assertEquals(arm1, pickAndPlace.getRobotToExecute());
    assertEquals(x, pickAndPlace.getObjectToPick());
    assertThat(pickAndPlace.getTargetLocation().containedInRegion()).contains(blue.realRegion());
  }

  @ParameterizedTest(name = "testIndirectMoveWith2Robots [emptyPreviousLocation = {argumentsWithNames}]")
  @ValueSource(booleans = {true, false})
  public void testIndirectMoveWith2Robots(boolean emptyPreviousLocation) {
    WorldModelB model = newModel();
    addMyObjects(model, "x");
    addMyLocationsAndRegions(model, "red", "blue", "green");
    addRobots(model, "arm1", "arm2");
    addReachability(model, "arm1", "red", "blue", "x");
    addReachability(model, "arm2", "blue", "green");

    LogicalScene logicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = logicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    LogicalRegion red = logicalScene.resolveLogicalObjectOfInterest("red").asLogicalRegion();
    LogicalRegion blue = logicalScene.resolveLogicalObjectOfInterest("blue").asLogicalRegion();
    LogicalRegion green = logicalScene.resolveLogicalObjectOfInterest("green").asLogicalRegion();
    Robot arm1 = model.findRobot("arm1").orElseThrow();
    Robot arm2 = model.findRobot("arm2").orElseThrow();

    // object "x" shall be place at red or green
    DifferenceObjectAtWrongPlace diff = createDifferenceObjectAtWrongPlace(x, emptyPreviousLocation ? null : red, green);
    List<Operation> operations = diff.computeOperations();

    assertThat(operations).isNotEmpty();
    assertThat(operations).hasSize(2);
    Operation op1 = operations.get(0);
    assertThat(op1).isInstanceOf(PickAndPlace.class);
    PickAndPlace pickAndPlace1 = (PickAndPlace) op1;
    assertEquals(arm1, pickAndPlace1.getRobotToExecute());
    assertEquals(x, pickAndPlace1.getObjectToPick());
    assertThat(pickAndPlace1.getTargetLocation().containedInRegion()).contains(blue.realRegion());

    Operation op2 = operations.get(1);
    assertThat(op2).isInstanceOf(PickAndPlace.class);
    PickAndPlace pickAndPlace2 = (PickAndPlace) op2;
    assertEquals(arm2, pickAndPlace2.getRobotToExecute());
    assertEquals(x, pickAndPlace2.getObjectToPick());
    assertThat(pickAndPlace2.getTargetLocation().containedInRegion()).contains(green.realRegion());
  }

  @ParameterizedTest(name = "testIndirectMoveWith3Robots [emptyPreviousLocation = {argumentsWithNames}]")
  @ValueSource(booleans = {true, false})
  public void testIndirectMoveWith3Robots(boolean emptyPreviousLocation) {
    /*
     * red -(arm1)-> blue --(arm3)-,
     *    `-(arm2)-> green <-------'
     *                 `--(arm4)-> yellow -(arm5)-> purple
     */
    WorldModelB model = newModel();
    addMyObjects(model, "x");
    addMyLocationsAndRegions(model, "red", "blue", "green", "yellow", "purple");
    addRobots(model, "arm1", "arm2", "arm3", "arm4", "arm5");
    addReachability(model, "arm1", "red", "blue", "x");
    addReachability(model, "arm2", "red", "green", "x");
    addReachability(model, "arm3", "blue", "green");
    addReachability(model, "arm4", "green", "yellow");
    addReachability(model, "arm5", "yellow", "purple");

    LogicalScene logicalScene = model.getMyScene().getLogicalScene();
    LogicalMovableObject x = logicalScene.resolveLogicalObjectOfInterest("x").asLogicalMovableObject();
    LogicalRegion red = logicalScene.resolveLogicalObjectOfInterest("red").asLogicalRegion();
    LogicalRegion green = logicalScene.resolveLogicalObjectOfInterest("green").asLogicalRegion();
    LogicalRegion yellow = logicalScene.resolveLogicalObjectOfInterest("yellow").asLogicalRegion();
    LogicalRegion purple = logicalScene.resolveLogicalObjectOfInterest("purple").asLogicalRegion();
    Robot arm2 = model.findRobot("arm2").orElseThrow();
    Robot arm4 = model.findRobot("arm4").orElseThrow();
    Robot arm5 = model.findRobot("arm5").orElseThrow();

    // object "x" shall be place at red or purple
    DifferenceObjectAtWrongPlace diff = createDifferenceObjectAtWrongPlace(x, emptyPreviousLocation ? null : red, purple);
    List<Operation> operations = diff.computeOperations();

    assertThat(operations).isNotEmpty();
    assertThat(operations).hasSize(3);

    Operation op1 = operations.get(0);
    assertThat(op1).isInstanceOf(PickAndPlace.class);
    PickAndPlace pickAndPlace1 = (PickAndPlace) op1;
    assertEquals(arm2, pickAndPlace1.getRobotToExecute());
    assertEquals(x, pickAndPlace1.getObjectToPick());
    assertThat(pickAndPlace1.getTargetLocation().containedInRegion()).contains(green.realRegion());

    Operation op2 = operations.get(1);
    assertThat(op2).isInstanceOf(PickAndPlace.class);
    PickAndPlace pickAndPlace2 = (PickAndPlace) op2;
    assertEquals(arm4, pickAndPlace2.getRobotToExecute());
    assertEquals(x, pickAndPlace2.getObjectToPick());
    assertThat(pickAndPlace2.getTargetLocation().containedInRegion()).contains(yellow.realRegion());

    Operation op3 = operations.get(2);
    assertThat(op3).isInstanceOf(PickAndPlace.class);
    PickAndPlace pickAndPlace3 = (PickAndPlace) op3;
    assertEquals(arm5, pickAndPlace3.getRobotToExecute());
    assertEquals(x, pickAndPlace3.getObjectToPick());
    assertThat(pickAndPlace3.getTargetLocation().containedInRegion()).contains(purple.realRegion());
  }

  private DifferenceObjectAtWrongPlace createDifferenceObjectAtWrongPlace(LogicalMovableObject x, LogicalRegion previousRegion, LogicalRegion newRegion) {
    DifferenceObjectAtWrongPlace result = new DifferenceObjectAtWrongPlace();
    result.setObject(x);
    result.setPreviousRegion(previousRegion);
    result.setNewRegion(newRegion);
    return result;
  }
}
