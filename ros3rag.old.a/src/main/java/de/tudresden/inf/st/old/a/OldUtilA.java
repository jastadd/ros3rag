package de.tudresden.inf.st.old.a;

import de.tudresden.inf.st.old.a.ast.*;
import de.tudresden.inf.st.ros3rag.common.RegionConfiguration;
import de.tudresden.inf.st.ros3rag.common.RegionConfiguration.RegionDefinition;
import de.tudresden.inf.st.ros3rag.common.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;

/**
 * Static utility methods used only for place A.
 *
 * @author rschoene - Initial contribution
 */
public class OldUtilA {
  private static final Logger logger = LogManager.getLogger(OldUtilA.class);

  static Path pathToDirectoryOfPlaceA() {
    return Util.pathToModuleDirectory("ros3rag.old.a");
  }

  static Scene convert(de.tudresden.inf.st.ceti.Scene scene) throws Exception {
    return new ExposingASTNode().exposed_apply_ConvertScene(scene);
  }

  static void setRegions(WorldModelA model, RegionConfiguration config) {
    JastAddList<Region> result = new JastAddList<>();
    for (RegionDefinition def : config.regions) {
      Region region = new Region();
      region.setName(def.name);
      region.setLocationNames(String.join(",", def.positions));
      result.add(region);
    }
    model.setRegionList(result);
  }

  static void updatePositionOfObjectToLocation(Scene scene, String objName, String locationName) {
    ObjectOfInterest obj = scene.resolveObjectOfInterest(objName);
    ObjectOfInterest location = scene.resolveObjectOfInterest(locationName);
    if (obj != null && location != null) {
      // move objectRed1 to binBlue
      logger.info("Got " + obj + " and location " + location);
      logger.debug("before to {} at {}\n{}", locationName, location.getPosition(),
          scene.prettyPrint());

      obj.setPosition(Position.of(location.getPosition().getX(),
          location.getPosition().getY(),
          location.getPosition().getZ()));

      logger.debug("after\n{}", scene.prettyPrint());
    } else {
      logger.error("Obj (" + obj + ") or location (" + location + ") are null");
    }
  }

  public static String getModelInfos(WorldModelA model, boolean detailed) {
    StringBuilder sb = new StringBuilder();
    if (detailed) {
      // also include "normal" scene
      sb.append("myScene:");
      if (model.hasScene()) {
        sb.append("\n").append(model.getScene().prettyPrint());
      } else {
        sb.append(" (unset)\n");
      }
    }
    sb.append("myLogicalScene:");
    if (model.hasScene()) {
      sb.append("\n").append(model.getScene().getLogicalScene().prettyPrint());
    } else {
      sb.append(" (unset)\n");
    }
    return sb.toString();
  }

  @SuppressWarnings("rawtypes")
  static class ExposingASTNode extends ASTNode {
    public Scene exposed_apply_ConvertScene(de.tudresden.inf.st.ceti.Scene pbScene) throws Exception {
      return _apply_ConvertScene(pbScene);
    }
  }
}
