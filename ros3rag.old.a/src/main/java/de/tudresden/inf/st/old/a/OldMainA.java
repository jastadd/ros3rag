package de.tudresden.inf.st.old.a;

import de.tudresden.inf.st.old.a.ast.ArtificialRoot;
import de.tudresden.inf.st.old.a.ast.MqttHandler;
import de.tudresden.inf.st.old.a.ast.Scene;
import de.tudresden.inf.st.old.a.ast.WorldModelA;
import de.tudresden.inf.st.ros3rag.common.SharedMainParts;
import de.tudresden.inf.st.ros3rag.common.Util;

import java.io.File;
import java.io.IOException;

import static de.tudresden.inf.st.ros3rag.common.Util.mqttUri;
import static de.tudresden.inf.st.ros3rag.common.Util.readScene;

/**
 * Entry point for RAG model in place A.
 *
 * @author rschoene - Initial contribution
 */
public class OldMainA extends SharedMainParts<MqttHandler, ArtificialRoot> {
  private final String TOPIC_SCENE_UPDATE_FROM_ROS;
  private final String TOPIC_SCENE_UPDATE_TO_PLACE_B;

  private final String TOPIC_DEMO_MOVE_objectRed1_BLUE;
  private final String TOPIC_DEMO_MOVE_objectRed1_RED;
  private final String TOPIC_EVAL_MOVE;

  OldMainA(String configFile) {
    super("place-a", OldUtilA.pathToDirectoryOfPlaceA().resolve(configFile));
    this.TOPIC_SCENE_UPDATE_FROM_ROS = cellName + "/scene/update";
    this.TOPIC_SCENE_UPDATE_TO_PLACE_B = cellName + "/logical/update";

    this.TOPIC_DEMO_MOVE_objectRed1_BLUE = cellName + "/demo/move/objectRed1/blue";
    this.TOPIC_DEMO_MOVE_objectRed1_RED = cellName + "/demo/move/objectRed1/red";
    this.TOPIC_EVAL_MOVE = cellName + "/eval/move";
  }

  public static void main(String[] args) throws Exception {
    String configFile = args.length == 0 ? "src/main/resources/config-a.yaml" : args[0];
    new OldMainA(configFile).run();
  }

  @Override
  protected MqttHandler createMqttHandler() {
    return new MqttHandler("mainHandlerA");
  }

  @Override
  protected void createSpecificMainHandlerConnections() {
    mainHandler.newConnection(TOPIC_DEMO_MOVE_objectRed1_BLUE, bytes ->
        OldUtilA.updatePositionOfObjectToLocation(model.getWorldModelA().getScene(), "objectRed1", "binBlue")
    );
    mainHandler.newConnection(TOPIC_DEMO_MOVE_objectRed1_RED, bytes ->
        OldUtilA.updatePositionOfObjectToLocation(model.getWorldModelA().getScene(), "objectRed1", "binRed")
    );
    mainHandler.newConnection(TOPIC_EVAL_MOVE, bytes -> {
        String[] tokens = new String(bytes).split(" to ");
        String objectName = tokens[0];
        String locationName = tokens[1];
        OldUtilA.updatePositionOfObjectToLocation(model.getWorldModelA().getScene(), objectName, locationName);
    });
  }

  @Override
  protected ArtificialRoot createWorldModel() throws Exception {
    de.tudresden.inf.st.ceti.Scene scene = readScene(
        OldUtilA.pathToDirectoryOfPlaceA().resolve(config.forA.filenameInitialScene)
    );
    Scene myScene = OldUtilA.convert(scene);
    WorldModelA worldModel = new WorldModelA().setScene(myScene);
    return new ArtificialRoot().setWorldModelA(worldModel);
  }

  @Override
  protected void readInitialConfigs() throws IOException {
    // read and set regions
    File regionFile = OldUtilA.pathToDirectoryOfPlaceA().resolve(config.filenameRegions).toFile();
    OldUtilA.setRegions(model.getWorldModelA(), Util.parseRegionConfig(regionFile));

    // no robots to be set
  }

  @Override
  protected void connectEndpoints() throws IOException {
//    model.connectScene(mqttUri(TOPIC_SCENE_UPDATE_FROM_ROS, config));
    model.getWorldModelA().connectLogicalSceneAsNtaToken(mqttUri(TOPIC_SCENE_UPDATE_TO_PLACE_B, config), true);
  }

  @Override
  protected String getModelInfos(ArtificialRoot artificialRoot, boolean detailed) {
    return OldUtilA.getModelInfos(artificialRoot.getWorldModelA(), detailed);
  }
}
