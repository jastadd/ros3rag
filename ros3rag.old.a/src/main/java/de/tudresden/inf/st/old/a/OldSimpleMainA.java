package de.tudresden.inf.st.old.a;

import de.tudresden.inf.st.old.a.ast.*;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static de.tudresden.inf.st.ros3rag.common.Util.readScene;

/**
 * Testing features for placeA.
 *
 * @author rschoene - Initial contribution
 */
public class OldSimpleMainA {
  private static final Logger logger = LogManager.getLogger(OldSimpleMainA.class);

  @SuppressWarnings("CommentedOutCode")
  public static void main(String[] args) throws Exception {
//    testLocatedAt();
//    testBuildModelA();
    testReadSceneConfig();
  }

  @SuppressWarnings("CommentedOutCode")
  private static void testReadSceneConfig() throws Exception {
    final var path = OldUtilA.pathToDirectoryOfPlaceA().resolve("src/main/resources/config-scene-a.json");
    de.tudresden.inf.st.ceti.Scene scene = readScene(path);
    Scene myScene = OldUtilA.convert(scene);

    ArtificialRoot root = new ArtificialRoot();

    WorldModelA model = new WorldModelA();
    model.setScene(myScene);
    root.setWorldModelA(model);

    // send initial scene once
//    MqttHandler publisher = new MqttHandler().dontSendWelcomeMessage().setHost("localhost");
//    publisher.publish("scene/init", scene.toByteArray());
    logger.fatal("Skipping publishing to scene/init for now");

    root.ragconnectCheckIncremental();
    root.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

//    model.connectScene("mqtt://localhost/scene/update");
    model.connectLogicalSceneAsNtaToken("mqtt://localhost/logical/scene/update", true);
  }

  @SuppressWarnings("unused")
  private static void testBuildModelA() {
    WorldModelA model = new WorldModelA();

    // scene
    var scene = new Scene();
    model.setScene(scene);
    scene.addMovableObject(MovableObject.of("obj1", Position.of(1, 2, 3)));
    scene.addDropOffLocation(DropOffLocation.of("placeAlfa",
        Position.of(0, 0, 0),
        Orientation.of(0, 0, 0, 1),
        Size.of(1, 1, 1)));
    scene.addDropOffLocation(DropOffLocation.of("placeBeta",
        Position.of(1, 1, 1),
        Orientation.of(0, 0, 0, 1),
        Size.of(1, 1, 1)));
    scene.addDropOffLocation(DropOffLocation.of("placeGamma",
        Position.of(2, 2, 2),
        Orientation.of(0, 0, 0, 1),
        Size.of(2, 2, 2)));
    logger.info("obj1 at gamma: {}",
        scene.getMovableObject(0).isLocatedAt(scene.getDropOffLocation(2)));

    // check logical scene
    var logicalScene = scene.getLogicalScene();
    logger.info("logicalScene:\n{}", logicalScene.prettyPrint());
  }

  @SuppressWarnings("unused")
  private static void testLocatedAt() {
    Rotation r = new Rotation(0, 0, 1, 0, false);
    Vector3D v = new Vector3D(1, 2, 3);
    r.applyTo(v);

    var modelA = new WorldModelA();
    var sceneA = new Scene();
    modelA.setScene(sceneA);

    // rotate bin2 by 90°, so width and height should effectively be swapped afterwards
    Vector3D start = new Vector3D(1, 0, 0);
    Vector3D end = new Vector3D(0, 0, 1);
    Rotation rot = new Rotation(start, end);

    DropOffLocation bin1 = DropOffLocation.of("Bin1",
        Position.of(1, 1, 1),
        Orientation.of(0, 0, 0, 1),
        Size.of(2, 3, 4));
    DropOffLocation bin2 = DropOffLocation.of("Bin2",
        Position.of(1, 1, 1),
        Orientation.of((float) rot.getQ1(), (float) rot.getQ2(), (float) rot.getQ3(), (float) rot.getQ0()),
        Size.of(2, 3, 4));
    MovableObject objectA = MovableObject.of("ObjectA",
        Position.of(0, 0, 0));
    MovableObject objectB = MovableObject.of("ObjectB",
        Position.of(1, 1, 1));
    sceneA.addDropOffLocation(bin1)
        .addMovableObject(objectA)
        .addMovableObject(objectB);

    /*
     0.0 <= x <= 2.0
    -0.5 <= y <= 2.5
    -1.0 <= z <= 3.0
     */

    logger.info("ObjectA locatedAt Bin1: {}", objectA.isLocatedAt(bin1));
    logger.info("ObjectB locatedAt Bin1: {}", objectB.isLocatedAt(bin1));
    logger.info("ObjectA locatedAt Bin2: {}", objectA.isLocatedAt(bin2));
    logger.info("ObjectB locatedAt Bin2: {}", objectB.isLocatedAt(bin2));
    Scanner scanner = new Scanner(System.in);
    while (true) {
      try {
        System.out.print("x: ");
        float x = scanner.nextFloat();
        System.out.print("y: ");
        float y = scanner.nextFloat();
        System.out.print("z: ");
        float z = scanner.nextFloat();
        MovableObject object = MovableObject.of("temp", Position.of(x, y, z));
        System.out.println("located at Bin2? : " + object.isLocatedAt(bin2));
      } catch (Exception e) {
        break;
      }
    }
  }
}
