# MODELS 2021 Paper - Codename 'Ros3Rag'

Part of <https://git-st.inf.tu-dresden.de/ceti/ros/models2021> (to be created)

Publication: <https://git-st.inf.tu-dresden.de/stgroup/publications/2021/ragconnect-followup>
